# Use go 1.x based on alpine image.
FROM golang:1.19.4-alpine AS build

ADD . /go/src/debt-request-process/
ENV GO111MODULE on
WORKDIR /go/src/debt-request-process
RUN go mod download
RUN go build -o dist/bin/debt-request-process ./cmd/debt-request-process

# Release binary on latest alpine image.
FROM alpine:latest

COPY --from=build /go/src/debt-request-process/dist/bin/debt-request-process /usr/local/bin/debt-request-process
COPY --from=build /go/src/debt-request-process/html/templates/callback.html /html/templates/callback.html
COPY --from=build /go/src/debt-request-process/html/templates/debt-request-process.html /html/templates/debt-request-process.html

ENV HTML_TEMPLATE_DIRECTORY "/html/templates"

# Add non-priveleged user
RUN adduser -D -u 1001 appuser
USER appuser
CMD ["/usr/local/bin/debt-request-process"]
