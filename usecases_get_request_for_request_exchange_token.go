// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package debtrequestprocess

import (
	"errors"
	"fmt"
)

var ErrRequestExchangeTokenInvalid = errors.New("invalid exchange token")

func (a *UseCases) GetDebtRequest(requestExchangeToken string) (*DebtRequest, error) {
	requestId, err := a.requestExchangeTokenRepository.GetRequestIdForRequestExchangeToken(requestExchangeToken)
	if err != nil {
		return nil, fmt.Errorf("failed to get request id: %v", err)
	}

	if requestId == "" {
		return nil, ErrRequestExchangeTokenInvalid
	}

	return a.debtRequestRepository.Get(requestId)
}
