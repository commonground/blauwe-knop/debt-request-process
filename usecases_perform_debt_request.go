// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package debtrequestprocess

import (
	"encoding/pem"
	"errors"
	"fmt"
	"log"
)

func (s *DebtRequest) GetOrganizationNames() []string {
	names := make([]string, len(s.Organizations))
	for i, org := range s.Organizations {
		names[i] = org.Name
	}

	return names
}

var ErrOrganizationNotInScheme = errors.New("oin is not present in scheme")
var ErrPerformDebtRequestRequestExchangeTokenInvalid = errors.New("invalid exchange token")
var ErrGettingDebtRequestForRequestId = errors.New("failed to fetch debt request for request id")
var ErrInvalidPublicKeyPEM = errors.New("invalid public key")

func (a *UseCases) PerformDebtRequest(oins []string, publicKeyPEM string) (*DebtRequest, string, error) {
	block, rest := pem.Decode([]byte(publicKeyPEM))
	log.Printf("pem decode, rest: %v", rest)
	log.Printf("pem decode, block: %v", block)
	if block == nil {
		return nil, "", ErrInvalidPublicKeyPEM
	}

	var organizations []*Organization
	for _, oin := range oins {
		org, err := a.schemeRepository.GetOrganizationByOIN(oin)

		if err != nil {
			return nil, "", fmt.Errorf("failed to get organization by oin: %v", err)
		}

		if org == nil {
			return nil, "", ErrOrganizationNotInScheme
		}

		organizations = append(organizations, org)
	}

	req := &DebtRequest{
		Organizations: organizations,
		PublicKeyPEM:  publicKeyPEM,
	}

	debtRequest, err := a.debtRequestRepository.Create(*req)
	if err != nil {
		return nil, "", fmt.Errorf("failed to save debt request: %v", err)
	}

	requestExchangeToken := a.tokenGenerator.GenerateRequestExchangeToken()
	err = a.requestExchangeTokenRepository.AddRequestExchangeToken(debtRequest.Id, requestExchangeToken)
	if err != nil {
		return nil, "", fmt.Errorf("failed to add request exchange token: %v", err)
	}
	return debtRequest, requestExchangeToken, nil
}

func (a *UseCases) AddBSNToRequest(BSN string, requestExchangeToken string) (*DebtRequest, error) {
	requestId, err := a.requestExchangeTokenRepository.GetRequestIdForRequestExchangeToken(requestExchangeToken)
	if err != nil {
		return nil, fmt.Errorf("failed to get request id: %v", err)
	}

	if requestId == "" {
		return nil, ErrPerformDebtRequestRequestExchangeTokenInvalid
	}

	debtRequest, err := a.debtRequestRepository.Get(requestId)
	if err != nil {
		log.Printf("error getting debt request by id: %v", err)
		return nil, ErrGettingDebtRequestForRequestId
	}

	if debtRequest == nil {
		log.Printf("no debt request for request by id: %v", err)
		return nil, ErrGettingDebtRequestForRequestId
	}

	debtRequest.BSN = BSN
	err = a.debtRequestRepository.Update(debtRequest.Id, *debtRequest)
	if err != nil {
		return nil, err
	}

	return debtRequest, nil
}
