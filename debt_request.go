// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package debtrequestprocess

type DebtRequest struct {
	Id            string
	BSN           string
	PublicKeyPEM  string
	Organizations []*Organization
}
