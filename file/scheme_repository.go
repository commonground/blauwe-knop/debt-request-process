// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package file

import (
	"debtrequestprocess"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"time"

	healthcheck "gitlab.com/commonground/blauwe-knop/health-checker/healthchecker"
)

type SchemeRepository struct {
	organizations []*debtrequestprocess.Organization
}

func NewSchemeRepository(filePath string) (*SchemeRepository, error) {
	organizations, err := readScheme(filePath)
	if err != nil {
		return nil, fmt.Errorf("error reading scheme: %v", err)
	}
	return &SchemeRepository{
		organizations: organizations,
	}, nil
}

func readScheme(filePath string) ([]*debtrequestprocess.Organization, error) {
	fileBytes, err := ioutil.ReadFile(filePath)
	if err != nil {
		return nil, fmt.Errorf("failed to read file: %v", err)
	}

	var organizations = []*debtrequestprocess.Organization{}
	err = json.Unmarshal(fileBytes, &organizations)
	if err != nil {
		return nil, fmt.Errorf("failed to unmarshal json: %v", err)
	}

	return organizations, nil
}

func (s *SchemeRepository) GetOrganizationByOIN(oin string) (*debtrequestprocess.Organization, error) {
	for _, organization := range s.organizations {
		if organization.OIN == oin {
			return organization, nil
		}
	}

	return nil, nil
}

func (s *SchemeRepository) GetHealthCheck() healthcheck.Result {
	name := "scheme"
	start := time.Now()

	return healthcheck.Result{
		Name:         name,
		Status:       healthcheck.StatusOK,
		ResponseTime: time.Since(start).Seconds(),
		HealthChecks: nil,
	}
}
