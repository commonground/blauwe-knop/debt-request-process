// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package http

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"debtrequestprocess"

	healthcheck "gitlab.com/commonground/blauwe-knop/health-checker/healthchecker"
)

type createDebtRequestOrganizationRequestModel struct {
	OIN        string `json:"oin"`
	Name       string `json:"name"`
	APIBaseURL string `json:"apiBaseUrl"`
	LoginURL   string `json:"loginUrl"`
}

type createDebtRequestRequestModel struct {
	Id            string                                      `json:"id"`
	BSN           string                                      `json:"bsn"`
	Organizations []createDebtRequestOrganizationRequestModel `json:"organizations"`
}

type debtRequestResponseModel struct {
	Id            string `json:"id"`
	BSN           string `json:"bsn"`
	Organizations []*struct {
		OIN        string `json:"oin"`
		Name       string `json:"name"`
		APIBaseURL string `json:"apiBaseUrl"`
		LoginURL   string `json:"loginUrl"`
	} `json:"organizations"`
}

type DebtRequestRepository struct {
	baseURL string
	apiKey  string
}

func NewDebtRequestRegisterRepository(baseURL string, apiKey string) *DebtRequestRepository {
	return &DebtRequestRepository{
		baseURL: baseURL,
		apiKey:  apiKey,
	}
}

func (s *DebtRequestRepository) Get(debtRequestID string) (*debtrequestprocess.DebtRequest, error) {
	url := fmt.Sprintf("%s/debt-requests/%s", s.baseURL, debtRequestID)
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return nil, fmt.Errorf("failed to create get debt-request request: %v", err)
	}

	req.Header.Set("Authentication", s.apiKey)

	client := &http.Client{Timeout: 20 * time.Second}
	resp, err := client.Do(req)
	if err != nil {
		return nil, fmt.Errorf("failed to fetch debt request: %v", err)
	}

	if resp.StatusCode == http.StatusNotFound {
		return nil, nil
	} else if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected status code while fetching debt request: %d", resp.StatusCode)
	}

	debtRequestResponse := &debtRequestResponseModel{}
	err = json.NewDecoder(resp.Body).Decode(debtRequestResponse)
	if err != nil {
		return nil, fmt.Errorf("failed to decode json: %v", err)
	}

	return mapDebtRequestResponseToModel(debtRequestResponse), nil
}

func mapModelToCreateDebtRequestRequest(debtRequest debtrequestprocess.DebtRequest) createDebtRequestRequestModel {
	result := createDebtRequestRequestModel{
		BSN: debtRequest.BSN,
	}

	for _, v := range debtRequest.Organizations {
		result.Organizations = append(result.Organizations, createDebtRequestOrganizationRequestModel{
			OIN:        v.OIN,
			Name:       v.Name,
			LoginURL:   v.LoginURL,
			APIBaseURL: v.APIBaseURL,
		})
	}

	return result
}

func mapDebtRequestResponseToModel(response *debtRequestResponseModel) *debtrequestprocess.DebtRequest {
	result := debtrequestprocess.DebtRequest{
		Id:  response.Id,
		BSN: response.BSN,
	}

	for _, v := range response.Organizations {
		result.Organizations = append(result.Organizations, &debtrequestprocess.Organization{
			OIN:        v.OIN,
			Name:       v.Name,
			LoginURL:   v.LoginURL,
			APIBaseURL: v.APIBaseURL,
		})
	}

	return &result
}

func (s *DebtRequestRepository) Create(debtRequest debtrequestprocess.DebtRequest) (*debtrequestprocess.DebtRequest, error) {
	requestBody := mapModelToCreateDebtRequestRequest(debtRequest)
	requestBodyAsJson, err := json.Marshal(requestBody)
	if err != nil {
		return nil, fmt.Errorf("failed to marshall request body: %v", err)
	}

	url := fmt.Sprintf("%s/debt-requests", s.baseURL)
	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(requestBodyAsJson))
	if err != nil {
		return nil, fmt.Errorf("failed to create get link token request: %v", err)
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authentication", s.apiKey)

	client := &http.Client{Timeout: 20 * time.Second}
	resp, err := client.Do(req)
	if err != nil {
		return nil, fmt.Errorf("failed to create debt request: %v", err)
	}

	if resp.StatusCode != http.StatusCreated {
		return nil, fmt.Errorf("unexpected status while creating debt request: %d", resp.StatusCode)
	}

	debtRequestResponse := &debtRequestResponseModel{}
	err = json.NewDecoder(resp.Body).Decode(debtRequestResponse)
	if err != nil {
		return nil, fmt.Errorf("failed to decode json: %v", err)
	}

	return mapDebtRequestResponseToModel(debtRequestResponse), nil
}

func (s *DebtRequestRepository) Update(id string, debtRequest debtrequestprocess.DebtRequest) error {
	requestBody := mapModelToCreateDebtRequestRequest(debtRequest)
	requestBodyAsJson, err := json.Marshal(requestBody)
	if err != nil {
		return fmt.Errorf("failed to marshall request body: %v", err)
	}

	url := fmt.Sprintf("%s/debt-requests/%s", s.baseURL, id)
	req, err := http.NewRequest(http.MethodPut, url, bytes.NewBuffer(requestBodyAsJson))
	if err != nil {
		return fmt.Errorf("failed to create get link token request: %v", err)
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authentication", s.apiKey)

	client := &http.Client{Timeout: 20 * time.Second}
	resp, err := client.Do(req)
	if err != nil {
		return fmt.Errorf("failed to update debt request: %v", err)
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("unexpected status while updating debt request: %d", resp.StatusCode)
	}

	debtRequestResponse := &debtRequestResponseModel{}
	err = json.NewDecoder(resp.Body).Decode(debtRequestResponse)
	if err != nil {
		return fmt.Errorf("failed to decode json: %v", err)
	}

	return nil
}

func (s *DebtRequestRepository) GetHealthCheck() healthcheck.Result {
	name := "debt-request-register"
	url := fmt.Sprintf("%s/health/check", s.baseURL)
	timeout := 10 * time.Second
	start := time.Now()

	client := http.Client{
		Timeout: timeout,
	}

	request, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		log.Printf("failed to create health check request: %v", err)
		return healthcheck.Result{
			Name:         name,
			Status:       healthcheck.StatusError,
			ResponseTime: time.Since(start).Seconds(),
			HealthChecks: nil,
		}
	}

	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Authentication", s.apiKey)

	response, err := client.Do(request)
	if err != nil {
		log.Printf("failed to get health check request: %v", err)
		return healthcheck.Result{
			Name:         name,
			Status:       healthcheck.StatusError,
			ResponseTime: time.Since(start).Seconds(),
			HealthChecks: nil,
		}
	}

	body, err := ioutil.ReadAll(response.Body)

	if response.StatusCode != http.StatusOK && response.StatusCode != http.StatusServiceUnavailable {
		if err != nil {
			log.Printf("failed to parse body health check request: %s", err.Error())
		} else {
			log.Printf("failed to parse body health check request: %d - %s", response.StatusCode, string(body))
		}

		return healthcheck.Result{
			Name:         name,
			Status:       healthcheck.StatusError,
			ResponseTime: time.Since(start).Seconds(),
			HealthChecks: nil,
		}
	}
	log.Printf("resp.Body: %s", string(body))

	var healthCheckResponse healthcheck.Result

	err = json.Unmarshal(body, &healthCheckResponse)
	if err != nil {
		log.Printf("Unmarshal error: %v", err)
		healthCheckResponse.Status = healthcheck.StatusError
	}

	return healthCheckResponse
}
