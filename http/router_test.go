// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package http_test

import (
	"testing"

	"github.com/golang/mock/gomock"

	"debtrequestprocess"
	"debtrequestprocess/mock"
)

const DummyOrganizationOIN = "00000000000000000000"

var DummyOrganization = debtrequestprocess.Organization{
	OIN:        DummyOrganizationOIN,
	Name:       "dummy-organization",
	APIBaseURL: "https://dummy-organization.com/",
	LoginURL:   "https://dummy-organization.com/login",
}

func generateRequestExchangeTokenRepository(t *testing.T) *mock.MockRequestExchangeTokenRepository {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	repo := mock.NewMockRequestExchangeTokenRepository(ctrl)
	return repo
}

func generateDebtRequestRepository(t *testing.T) *mock.MockDebtRequestRepository {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	repo := mock.NewMockDebtRequestRepository(ctrl)
	return repo
}

func generateSchemeRepo(t *testing.T) *mock.MockSchemeRepository {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	repo := mock.NewMockSchemeRepository(ctrl)
	return repo
}

func generateTokenGeneratorRepo(t *testing.T) *mock.MockTokenGenerator {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	repo := mock.NewMockTokenGenerator(ctrl)
	return repo
}

func generateLoginService(t *testing.T) *mock.MockLoginService {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	repo := mock.NewMockLoginService(ctrl)
	return repo
}

const dummyPublicKeyPem string = `-----BEGIN PUBLIC KEY-----
MIICCgKCAgEAxIxQg/R55cEDFNnmV2MGw3OQ+E4kS48D4MhpKV4QDn7jKhMbQsIGSeOAWoEAN10ATSwnkdnRIBkQe2kZglsGsZWM4pb+CamKZEtB2BSNcSDHpXPuq8CbUM/cvAXhM/Di6/idd2Gf3J4wJvbb177wNgnLTa6GMO5vDXcrD1aIfUP8kyzly0lVKK1EqxBosIHqx0vOc6yg3gwEgjx2npr6JuRAElUpkIHCGiViMzJL9VqhXkSFrJ382aJV5O5h6apvV4xePbgJY5RIdVTzFqlq9fW7szewvXyl/66eupeMXLYVeC+0F/bcofozmq5ue+VXReFRyIxj0u62/aYYD6DcnHypALkBgR0AhphOb/KvKq5gs0ndX4ZmxieEO7Z4ZEJdA/w4Cul4foyaQzPqshVmOHoscjYrsdGneW4Bqupf7gjrjFWyYoyb/bbQEiHbnSu8eYTzGrzfrPfYHh2X2hXbM3nUZocLjAajQJXrxdAZTYyG6QRDgHaq9R3m+yoNXLU3bX6AALYgBoEEBnLJd3lHilELh8EYNeMC15wqOShd8wlM0qPUyHXjeeZMQMUVv7sgeuCB5N+pRMDamDEa8W1HXhCqXo7gN7GvvS4LcZIMnK9+PBYZMRiet9JzcF/ipTO5hO7YMNH9ZhoPHPHXfiCEg03cbWhekw/4TyGYubLFHWkCAwEAAQ==
-----END PUBLIC KEY-----`
