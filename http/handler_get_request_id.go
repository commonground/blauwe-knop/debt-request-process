// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package http

import (
	"log"
	"net/http"

	"github.com/go-chi/render"

	"debtrequestprocess"
)

func handlerGetRequestId(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	debtrequestprocessUseCase, _ := ctx.Value(debtrequestprocessUseCase).(*debtrequestprocess.UseCases)

	requestExchangeToken := r.URL.Query().Get("requestExchangeToken")
	if requestExchangeToken == "" {
		log.Printf("requestExchangeToken required")
		http.Error(w, "requestExchangeToken required", http.StatusBadRequest)
		return
	}

	request, err := debtrequestprocessUseCase.GetDebtRequest(requestExchangeToken)
	if err == debtrequestprocess.ErrRequestExchangeTokenInvalid {
		log.Printf("request exchange token is invalid %v", err)
		http.Error(w, "request exchange token is invalid", http.StatusBadRequest)
		return
	} else if err != nil {
		log.Printf("error exchanging token %v", err)
		http.Error(w, "error exchanging token", http.StatusInternalServerError)
		return
	}

	if request == nil {
		log.Printf("request does not exist")
		http.Error(w, "request does not exist", http.StatusNotFound)
		return
	}

	render.PlainText(w, r, request.Id)
}
