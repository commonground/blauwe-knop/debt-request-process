// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package http_test

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"debtrequestprocess"
	http_infra "debtrequestprocess/http"
)

func mockedGetCurrentTime() time.Time {
	return time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC)
}

func Test_CreateRouter_PerformDebtRequest(t *testing.T) {
	type fields struct {
		config http_infra.RouterConfig
	}
	type args struct {
		organizations string
		publicKeyPEM  string
	}
	tests := []struct {
		name                   string
		fields                 fields
		args                   args
		expectedHTTPStatusCode int
		expectedBody           string
		expectedCookies        []*http.Cookie
	}{
		{
			"template file is corrupt / invalid",
			fields{
				http_infra.RouterConfig{
					OrganizationName:      "my-organization",
					LogoURL:               "https://my-organization.com/logo.svg",
					HTMLTemplateDirectory: "../testing/invalid-html-templates",
					CallbackURL:           "https://callback-baseURL.com",
					LoginServiceURL:       "https://service.com/login",
					DebtRequestUseCase: func() *debtrequestprocess.UseCases {
						requestExchangeTokenRepository := generateRequestExchangeTokenRepository(t)
						requestExchangeTokenRepository.EXPECT().AddRequestExchangeToken(gomock.Any(), gomock.Any()).Return(nil).AnyTimes()
						schemeRepository := generateSchemeRepo(t)
						schemeRepository.EXPECT().GetOrganizationByOIN(gomock.Any()).Return(&DummyOrganization, nil).AnyTimes()
						debtRequestRepository := generateDebtRequestRepository(t)
						tokenGenerator := generateTokenGeneratorRepo(t)
						tokenGenerator.EXPECT().GenerateRequestExchangeToken().Return("request-exchange-token").AnyTimes()

						result := debtrequestprocess.NewUseCases(
							requestExchangeTokenRepository,
							debtRequestRepository,
							schemeRepository,
							generateLoginService(t),
							tokenGenerator,
						)

						return result
					}(),
					GetCurrentTime: mockedGetCurrentTime,
				},
			},
			args{
				organizations: DummyOrganizationOIN,
				publicKeyPEM:  dummyPublicKeyPem,
			},
			http.StatusInternalServerError,
			"the page could not be loaded\n",
			[]*http.Cookie{},
		},
		{
			"template does not exist",
			fields{
				http_infra.RouterConfig{
					OrganizationName:      "my-organization",
					LogoURL:               "https://my-organization.com/logo.svg",
					HTMLTemplateDirectory: "non-existing-template-directory",
					DebtRequestUseCase: func() *debtrequestprocess.UseCases {
						requestExchangeTokenRepository := generateRequestExchangeTokenRepository(t)
						requestExchangeTokenRepository.EXPECT().AddRequestExchangeToken(gomock.Any(), gomock.Any()).Return(nil).AnyTimes()

						schemeRepository := generateSchemeRepo(t)
						schemeRepository.EXPECT().GetOrganizationByOIN(gomock.Any()).Return(&DummyOrganization, nil).AnyTimes()

						debtRequestRepository := generateDebtRequestRepository(t)

						tokenGenerator := generateTokenGeneratorRepo(t)
						tokenGenerator.EXPECT().GenerateRequestExchangeToken().Return("request-exchange-token").AnyTimes()

						result := debtrequestprocess.NewUseCases(
							requestExchangeTokenRepository,
							debtRequestRepository,
							schemeRepository,
							generateLoginService(t),
							tokenGenerator,
						)

						return result
					}(),
					GetCurrentTime: mockedGetCurrentTime,
				},
			},
			args{
				organizations: DummyOrganizationOIN,
				publicKeyPEM:  dummyPublicKeyPem,
			},
			http.StatusInternalServerError,
			"the page could not be loaded\n",
			[]*http.Cookie{},
		},
		{
			"without providing any organizations",
			fields{
				http_infra.RouterConfig{
					OrganizationName:      "my-organization",
					LogoURL:               "https://my-organization.com/logo.svg",
					HTMLTemplateDirectory: "../html/templates",
					DebtRequestUseCase: func() *debtrequestprocess.UseCases {
						requestExchangeTokenRepository := generateRequestExchangeTokenRepository(t)
						requestExchangeTokenRepository.EXPECT().AddRequestExchangeToken(gomock.Any(), gomock.Any()).Return(nil).AnyTimes()

						schemeRepository := generateSchemeRepo(t)
						schemeRepository.EXPECT().GetOrganizationByOIN(gomock.Any()).Return(&DummyOrganization, nil).AnyTimes()

						debtRequestRepository := generateDebtRequestRepository(t)

						tokenGenerator := generateTokenGeneratorRepo(t)
						tokenGenerator.EXPECT().GenerateRequestExchangeToken().Return("request-exchange-token").AnyTimes()

						result := debtrequestprocess.NewUseCases(
							requestExchangeTokenRepository,
							debtRequestRepository,
							schemeRepository,
							generateLoginService(t),
							tokenGenerator,
						)

						return result
					}(),
					GetCurrentTime: mockedGetCurrentTime,
				},
			},
			args{
				organizations: "",
				publicKeyPEM:  dummyPublicKeyPem,
			},
			http.StatusBadRequest,
			"no organizations provided\n",
			[]*http.Cookie{},
		},
		{
			"public key not provided",
			fields{
				http_infra.RouterConfig{
					OrganizationName:      "my-organization",
					LogoURL:               "https://my-organization.com/logo.svg",
					HTMLTemplateDirectory: "../html/templates",
					DebtRequestUseCase: func() *debtrequestprocess.UseCases {
						requestExchangeTokenRepository := generateRequestExchangeTokenRepository(t)
						requestExchangeTokenRepository.EXPECT().AddRequestExchangeToken(gomock.Any(), gomock.Any()).Return(nil).AnyTimes()

						schemeRepository := generateSchemeRepo(t)
						schemeRepository.EXPECT().GetOrganizationByOIN(gomock.Any()).Return(&DummyOrganization, nil).AnyTimes()

						debtRequestRepository := generateDebtRequestRepository(t)

						tokenGenerator := generateTokenGeneratorRepo(t)
						tokenGenerator.EXPECT().GenerateRequestExchangeToken().Return("request-exchange-token").AnyTimes()

						result := debtrequestprocess.NewUseCases(
							requestExchangeTokenRepository,
							debtRequestRepository,
							schemeRepository,
							generateLoginService(t),
							tokenGenerator,
						)

						return result
					}(),
					GetCurrentTime: mockedGetCurrentTime,
				},
			},
			args{
				organizations: DummyOrganizationOIN,
				publicKeyPEM:  "",
			},
			http.StatusBadRequest,
			"no public key provided\n",
			[]*http.Cookie{},
		},
		{
			"public key missing",
			fields{
				http_infra.RouterConfig{
					OrganizationName:      "my-organization",
					LogoURL:               "https://my-organization.com/logo.svg",
					HTMLTemplateDirectory: "../html/templates",
					DebtRequestUseCase: func() *debtrequestprocess.UseCases {
						requestExchangeTokenRepository := generateRequestExchangeTokenRepository(t)
						requestExchangeTokenRepository.EXPECT().AddRequestExchangeToken(gomock.Any(), gomock.Any()).Return(nil).AnyTimes()

						schemeRepository := generateSchemeRepo(t)
						schemeRepository.EXPECT().GetOrganizationByOIN(gomock.Any()).Return(&DummyOrganization, nil).AnyTimes()

						debtRequestRepository := generateDebtRequestRepository(t)

						tokenGenerator := generateTokenGeneratorRepo(t)
						tokenGenerator.EXPECT().GenerateRequestExchangeToken().Return("request-exchange-token").AnyTimes()

						result := debtrequestprocess.NewUseCases(
							requestExchangeTokenRepository,
							debtRequestRepository,
							schemeRepository,
							generateLoginService(t),
							tokenGenerator,
						)

						return result
					}(),
					GetCurrentTime: mockedGetCurrentTime,
				},
			},
			args{
				organizations: DummyOrganizationOIN,
				publicKeyPEM:  "invalid-public-key",
			},
			http.StatusBadRequest,
			"invalid public key\n",
			[]*http.Cookie{},
		},
		{
			"when the provided organization is not present in the scheme",
			fields{
				http_infra.RouterConfig{
					OrganizationName:      "my-organization",
					LogoURL:               "https://my-organization.com/logo.svg",
					HTMLTemplateDirectory: "../html/templates",
					DebtRequestUseCase: func() *debtrequestprocess.UseCases {
						requestExchangeTokenRepository := generateRequestExchangeTokenRepository(t)
						requestExchangeTokenRepository.EXPECT().AddRequestExchangeToken(gomock.Any(), gomock.Any()).Return(nil).AnyTimes()

						schemeRepository := generateSchemeRepo(t)
						schemeRepository.EXPECT().GetOrganizationByOIN(gomock.Any()).Return(nil, nil).AnyTimes()

						debtRequestRepository := generateDebtRequestRepository(t)

						tokenGenerator := generateTokenGeneratorRepo(t)
						tokenGenerator.EXPECT().GenerateRequestExchangeToken().Return("request-exchange-token").AnyTimes()

						result := debtrequestprocess.NewUseCases(
							requestExchangeTokenRepository,
							debtRequestRepository,
							schemeRepository,
							generateLoginService(t),
							tokenGenerator,
						)

						return result
					}(),
					GetCurrentTime: mockedGetCurrentTime,
				},
			},
			args{
				organizations: DummyOrganizationOIN,
				publicKeyPEM:  dummyPublicKeyPem,
			},
			http.StatusBadRequest,
			"request with non existing organization(s)\n",
			[]*http.Cookie{},
		},
		{
			"happy flow",
			fields{
				http_infra.RouterConfig{
					OrganizationName:      "my-organization",
					LogoURL:               "https://my-organization.com/logo.svg",
					HTMLTemplateDirectory: "../html/templates",
					DebtRequestUseCase: func() *debtrequestprocess.UseCases {
						requestExchangeTokenRepository := generateRequestExchangeTokenRepository(t)
						requestExchangeTokenRepository.EXPECT().AddRequestExchangeToken(gomock.Any(), gomock.Any()).Return(nil).AnyTimes()
						schemeRepository := generateSchemeRepo(t)
						schemeRepository.EXPECT().GetOrganizationByOIN(gomock.Any()).Return(&DummyOrganization, nil).AnyTimes()
						debtRequestRepository := generateDebtRequestRepository(t)
						debtRequestRepository.EXPECT().Create(gomock.Any()).Return(&debtrequestprocess.DebtRequest{
							Organizations: []*debtrequestprocess.Organization{
								{
									Name: "dummy-organization",
								},
							},
						}, nil).AnyTimes()
						tokenGenerator := generateTokenGeneratorRepo(t)
						tokenGenerator.EXPECT().GenerateRequestExchangeToken().Return("request-exchange-token").AnyTimes()

						result := debtrequestprocess.NewUseCases(
							requestExchangeTokenRepository,
							debtRequestRepository,
							schemeRepository,
							generateLoginService(t),
							tokenGenerator,
						)

						return result
					}(),
					GetCurrentTime: mockedGetCurrentTime,
				},
			},
			args{
				organizations: DummyOrganizationOIN,
				publicKeyPEM:  dummyPublicKeyPem,
			},
			http.StatusOK,
			"<!DOCTYPE html>\n<html>\n\n<head>\n    <meta charset=\"utf-8\" />\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\" />\n    <title>Login</title>\n\n    <link href=\"https://fonts.googleapis.com/css?family=Open+Sans:400,600\" rel=\"stylesheet\" type=\"text/css\">\n\n    <style>\n        html,\n        body {\n            font-size: 16px;\n            line-height: 1.5rem;\n            font-family: 'Open Sans', sans-serif;\n            color: #212121;\n            font-weight: 400;\n            padding: 0;\n            margin: 0;\n        }\n\n        .container {\n            padding: 0 1rem 1rem 1rem;\n        }\n\n        h1 {\n            font-size: 1.125rem;\n            font-weight: 600;\n        }\n\n        p {\n            font-size: 1rem;\n        }\n\n        label {\n            cursor: pointer;\n            border-radius: 5px;\n            background-color: #EFEFEF;\n            border: 1px solid #767676;\n            display: block;\n            margin-bottom: .5rem;\n            line-height: 2.75rem;\n        }\n\n        label input {\n            margin: 0 15px 0 15px;\n        }\n\n        a.login-button {\n            display: flex;\n            text-decoration: none;\n        }\n\n        a.login-button span {\n            display: block;\n            cursor: pointer;\n            border: 0 none;\n            border-radius: 5px;\n            color: #ffffff;\n            text-align: center;\n            line-height: 2.75rem;\n            width: 100%;\n            max-width: 100%;\n            font-size: 1rem;\n            font-family: 'Open Sans', sans-serif;\n            background-color: #063D63;\n        }\n\n        a.login-button .digid-logo {\n            flex: 0 0 3.25rem;\n        }\n\n        .digid-logo {\n            display: block;\n            width: 2.75rem;\n            height: 2.75rem;\n            background-image: url('data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAxNTAgMTUwIj4KICAgIDxwYXRoIGQ9Ik0xMzYgMTUwSDE0Yy04IDAtMTQtNi0xNC0xNFYxNEMwIDYgNiAwIDE0IDBoMTIyYzggMCAxNCA2IDE0IDE0djEyMmMwIDgtNiAxNC0xNCAxNHoiLz4KICAgIDxwYXRoIGQ9Ik0xNyAxMTVWNzloMTBjMTIgMCAxOSA2IDE5IDE3IDAgMTMtOCAxOS0xOSAxOUgxN3ptNi02aDRjNyAwIDEyLTQgMTItMTMgMC04LTUtMTItMTMtMTJoLTN2MjV6TTU0IDc3YzMgMCA0IDEgNCAzcy0xIDQtNCA0Yy0yIDAtMy0yLTMtNHMxLTMgMy0zem0zIDM4aC02Vjg4aDZ2Mjd6TTcyIDEwOWg2YzYgMCA5IDMgOSA3IDAgNS00IDktMTQgOS04IDAtMTEtMi0xMS03IDAtMiAxLTQgNC02bC0yLTNjMC0yIDEtMyAzLTQtMy0yLTQtNC00LTggMC02IDQtMTAgMTEtMTBsNCAxaDl2NGgtNGwyIDVjMCA2LTQgOS0xMiA5aC0zbC0xIDFjMCAyIDEgMiAzIDJ6bTEgMTJjNiAwIDgtMiA4LTRzLTEtMi0zLTJsLTktMS0yIDNjMCAyIDIgNCA2IDR6bTYtMjRjMC0zLTItNS01LTVzLTUgMS01IDUgMSA1IDUgNWMzIDAgNS0xIDUtNXoiIGZpbGw9IiNmZmYiLz4KICAgIDxwYXRoIGQ9Ik05NCA3N2MyIDAgMyAxIDMgM3MtMSA0LTMgNGMtMyAwLTQtMi00LTRzMS0zIDQtM3ptMyAzOGgtNlY4OGg2djI3ek0xMDUgMTE1Vjc5aDEwYzEyIDAgMTggNiAxOCAxNyAwIDEzLTcgMTktMTkgMTloLTl6bTYtNmg0YzcgMCAxMi00IDEyLTEzIDAtOC01LTEyLTEzLTEyaC0zdjI1eiIgZmlsbD0iI0UxNzAwMCIvPgo8L3N2Zz4K');\n            background-repeat: no-repeat;\n            background-position: center right;\n        }\n\n        .collapsible-organizations {\n            position: relative;\n            border-top: 1px solid #E0E0E0;\n            border-bottom: 1px solid #E0E0E0;\n            padding: .75rem 0;\n            margin-bottom: 2rem;\n        }\n\n        .collapsible-organizations a {\n            text-decoration: none;\n            color: #212121;\n            display: block;\n        }\n\n        .collapsible-organizations ul {\n            display: none;\n        }\n\n        .collapsible-organizations::before {\n            content: ' ';\n            position: absolute;\n            display: block;\n            width: 24px;\n            height: 24px;\n            background-image: url('data:image/svg+xml;base64,PHN2ZyB2aWV3Qm94PSIwIDAgMjQgMjQiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgZmlsbC1ydWxlPSJldmVub2RkIiBjbGlwLXJ1bGU9ImV2ZW5vZGQiIHN0cm9rZS1saW5lam9pbj0icm91bmQiIHN0cm9rZS1taXRlcmxpbWl0PSIyIj48cGF0aCBkPSJNMTIgMTAuOTM5bC00Ljk1IDQuOTUtMS40MTQtMS40MTRMMTIgOC4xMTFsNi4zNjQgNi4zNjQtMS40MTQgMS40MTQtNC45NS00Ljk1eiIgZmlsbC1ydWxlPSJub256ZXJvIi8+PC9zdmc+');\n            background-position: center;\n            background-repeat: no-repeat;\n            fill: #6a6a6a;\n            transform: rotate(180deg);\n            top: .75rem;\n            right: 0;\n            transition: 150ms ease-in-out;\n        }\n\n        .collapsible-organizations.is-open::before {\n            transform: rotate(0deg);\n        }\n\n        .collapsible-organizations.is-open ul {\n            display: block;\n        }\n    </style>\n</head>\n\n<body>\n\n    <img src=\"https://my-organization.com/logo.svg\" style=\"max-height: 75px;max-width: 100%;\">\n    <div class=\"container\">\n        <h1>Bevestig uw verzoek</h1>\n\n        <div id=\"login-page\">\n            <p>U doet een verzoek tot het ophalen van schuldinformatie bij:</p>\n\n            <div class=\"collapsible-organizations\">\n                <a href=\"#\">1 overheidsorganisaties</a>\n                <ul>\n                    \n                        <li>dummy-organization</li>\n                    \n                </ul>\n            </div>\n\n            <p>Log in om verder te gaan.</p>\n\n\n            <a class=\"login-button\" href=\"?callbackURL=&amp;organizationName=my-organization\">\n                <span>Inloggen met DigiD</span>\n                <div class=\"digid-logo\"></div>\n            </a>\n        </div>\n    </div>\n\n    <script>\n      var collapsibleOrganizationsElement = document.querySelector('.collapsible-organizations')\n      var collapsibleOrganizationsLinkElement = collapsibleOrganizationsElement.querySelector('a')\n      collapsibleOrganizationsLinkElement.addEventListener('click', function (event) {\n        event.preventDefault()\n        collapsibleOrganizationsElement.classList.toggle('is-open')\n      })\n    </script>\n\n</body>\n\n</html>\n",
			[]*http.Cookie{
				{
					Name:       "requestExchangeToken",
					Value:      "request-exchange-token",
					Expires:    mockedGetCurrentTime().Add(10 * time.Minute),
					SameSite:   http.SameSiteStrictMode,
					RawExpires: "Wed, 01 Jan 2020 00:10:00 GMT",
					Raw:        "requestExchangeToken=request-exchange-token; Expires=Wed, 01 Jan 2020 00:10:00 GMT; SameSite=Strict",
				},
			},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			router, err := http_infra.NewRouter(test.fields.config)
			assert.Nil(t, err)
			w := httptest.NewRecorder()

			urlEncodedPublicKey := url.QueryEscape(test.args.publicKeyPEM)
			request := httptest.NewRequest("GET", fmt.Sprintf("/debt-request-process?organizations=%s&publicKey=%s", test.args.organizations, urlEncodedPublicKey), nil)
			router.ServeHTTP(w, request)

			resp := w.Result()
			body, _ := ioutil.ReadAll(resp.Body)

			assert.Equal(t, test.expectedHTTPStatusCode, resp.StatusCode)
			assert.Equal(t, test.expectedBody, string(body))
			assert.Equal(t, test.expectedCookies, resp.Cookies())
		})
	}
}
