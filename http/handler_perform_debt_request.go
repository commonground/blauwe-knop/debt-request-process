// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package http

import (
	"html/template"
	"log"
	"net/http"
	"net/url"
	"path"
	"path/filepath"
	"strings"
	"time"

	"debtrequestprocess"
)

type performDebtRequestPage struct {
	Name              string
	LogoURL           string
	OrganizationNames []string
	LoginURL          string
}

func handlerPerformDebtRequest(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	templateDirectory, _ := ctx.Value(htmlTemplateDirectoryKey).(string)
	debtrequestprocessUseCase, _ := ctx.Value(debtrequestprocessUseCase).(*debtrequestprocess.UseCases)
	callBackURL, _ := ctx.Value(callbackURLKey).(string)
	organizationName, _ := ctx.Value(organizationNameKey).(string)
	organizationLogoURL, _ := ctx.Value(organizationLogoUrlKey).(string)
	loginServiceURL, _ := ctx.Value(loginServiceURLKey).(string)
	getCurrentTime, _ := ctx.Value(getCurrentTimeKey).(func() time.Time)

	templatePath := filepath.Join(templateDirectory, "debt-request-process.html")
	filePath := path.Join(templatePath)

	publicKeyPEM := r.URL.Query().Get("publicKey")
	if publicKeyPEM == "" {
		log.Printf("no public key provided")
		http.Error(w, "no public key provided", http.StatusBadRequest)
		return
	}

	organizationsString := r.URL.Query().Get("organizations")
	if organizationsString == "" {
		log.Printf("no organizations provided")
		http.Error(w, "no organizations provided", http.StatusBadRequest)
		return
	}
	organizations := strings.Split(organizationsString, ",")

	parsedTemplate, err := template.ParseFiles(filePath)
	if err != nil {
		log.Printf("error parsing template: %v", err)
		http.Error(w, "the page could not be loaded", http.StatusInternalServerError)
		return
	}

	req, requestExchangeToken, err := debtrequestprocessUseCase.PerformDebtRequest(organizations, publicKeyPEM)
	if err == debtrequestprocess.ErrOrganizationNotInScheme {
		log.Printf("request with non existing organization(s)")
		http.Error(w, "request with non existing organization(s)", http.StatusBadRequest)
		return
	} else if err == debtrequestprocess.ErrInvalidPublicKeyPEM {
		log.Printf("invalid public key")
		http.Error(w, "invalid public key", http.StatusBadRequest)
		return
	} else if err != nil {
		log.Printf("error making request: %v", err)
		http.Error(w, "request could not be processed", http.StatusInternalServerError)
		return
	}

	expiration := getCurrentTime().Add(10 * time.Minute)
	cookie := http.Cookie{Name: "requestExchangeToken", Value: requestExchangeToken, Expires: expiration, SameSite: http.SameSiteStrictMode}
	http.SetCookie(w, &cookie)

	err = parsedTemplate.Execute(w, &performDebtRequestPage{
		Name:              organizationName,
		LogoURL:           organizationLogoURL,
		OrganizationNames: req.GetOrganizationNames(),
		LoginURL:          generateCallbackURL(loginServiceURL, callBackURL, organizationName),
	})

	if err != nil {
		log.Printf("error serving template: %v", err)
		http.Error(w, "the page could not be loaded", http.StatusInternalServerError)
		return
	}
}

func generateCallbackURL(loginServiceURL, callbackURL string, organizationName string) string {
	callbackURLEncoded := url.QueryEscape(callbackURL)
	u, _ := url.Parse(loginServiceURL)
	q := u.Query()
	q.Add("callbackURL", callbackURLEncoded)
	q.Add("organizationName", organizationName)
	u.RawQuery = q.Encode()

	return u.String()
}
