// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package http

import (
	"context"
	"errors"
	"net/http"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	healthcheck "gitlab.com/commonground/blauwe-knop/health-checker/healthchecker"

	"debtrequestprocess"
)

type RouterConfig struct {
	LogoURL               string
	OrganizationName      string
	HTMLTemplateDirectory string
	CallbackURL           string
	LoginServiceURL       string
	DebtRequestUseCase    *debtrequestprocess.UseCases
	GetCurrentTime        func() time.Time
}

func NewRouter(config RouterConfig) (*chi.Mux, error) {
	if len(config.OrganizationName) < 1 {
		return nil, errors.New("organization name is required for the router")
	}

	if len(config.LogoURL) < 1 {
		return nil, errors.New("organization logo url is required for the router")
	}

	r := chi.NewRouter()

	r.Use(middleware.Logger)

	r.Route("/debt-request-process", func(r chi.Router) {
		r.Get("/", func(w http.ResponseWriter, r *http.Request) {
			ctx := context.WithValue(r.Context(), htmlTemplateDirectoryKey, config.HTMLTemplateDirectory)
			ctx = context.WithValue(ctx, debtrequestprocessUseCase, config.DebtRequestUseCase)
			ctx = context.WithValue(ctx, callbackURLKey, config.CallbackURL)
			ctx = context.WithValue(ctx, loginServiceURLKey, config.LoginServiceURL)
			ctx = context.WithValue(ctx, getCurrentTimeKey, config.GetCurrentTime)
			ctx = context.WithValue(ctx, organizationNameKey, config.OrganizationName)
			ctx = context.WithValue(ctx, organizationLogoUrlKey, config.LogoURL)
			handlerPerformDebtRequest(w, r.WithContext(ctx))
		})
		r.Get("/callback", func(w http.ResponseWriter, r *http.Request) {
			ctx := context.WithValue(r.Context(), htmlTemplateDirectoryKey, config.HTMLTemplateDirectory)
			ctx = context.WithValue(ctx, debtrequestprocessUseCase, config.DebtRequestUseCase)
			ctx = context.WithValue(ctx, loginServiceURLKey, config.LoginServiceURL)
			ctx = context.WithValue(ctx, organizationNameKey, config.OrganizationName)
			ctx = context.WithValue(ctx, organizationLogoUrlKey, config.LogoURL)
			handlerCallBack(w, r.WithContext(ctx))
		})
		r.Get("/exchangetoken", func(w http.ResponseWriter, r *http.Request) {
			ctx := context.WithValue(r.Context(), debtrequestprocessUseCase, config.DebtRequestUseCase)
			handlerGetRequestId(w, r.WithContext(ctx))
		})
	})

	healthCheckHandler := healthcheck.NewHandler("debt-request-process", config.DebtRequestUseCase.GetHealthChecks())
	r.Route("/health", func(r chi.Router) {
		r.Get("/", healthCheckHandler.HandleHealth)
		r.Get("/check", healthCheckHandler.HandlerHealthCheck)
	})

	return r, nil
}

type key int

const (
	htmlTemplateDirectoryKey  key = iota
	debtrequestprocessUseCase key = iota
	organizationNameKey       key = iota
	organizationLogoUrlKey    key = iota
	callbackURLKey            key = iota
	loginServiceURLKey        key = iota
	getCurrentTimeKey         key = iota
)
