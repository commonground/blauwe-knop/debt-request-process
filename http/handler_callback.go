// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package http

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
	"path"
	"path/filepath"

	"debtrequestprocess"
)

type callback struct {
	Name              string
	LogoURL           string
	RedirectURL       template.URL
	OrganizationNames []string
}

func handlerCallBack(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	templateDirectory, _ := ctx.Value(htmlTemplateDirectoryKey).(string)
	debtrequestprocessUserCase, _ := ctx.Value(debtrequestprocessUseCase).(*debtrequestprocess.UseCases)
	organizationName, _ := ctx.Value(organizationNameKey).(string)
	organizationLogoURL, _ := ctx.Value(organizationLogoUrlKey).(string)
	bsn := r.URL.Query().Get("bsn")

	if bsn == "" {
		log.Printf("bsn required")
		http.Error(w, "bsn required", http.StatusBadRequest)
		return
	}

	templatePath := filepath.Join(templateDirectory, "callback.html")
	filePath := path.Join(templatePath)

	tmpl, err := template.ParseFiles(filePath)
	if err != nil {
		log.Printf("error parsing template: %v", err)
		http.Error(w, "the page could not be loaded", http.StatusInternalServerError)
		return
	}

	cookie, err := r.Cookie("requestExchangeToken")
	if err != nil {
		log.Printf("requestExchangeToken cookie is missing or expired - the page could not be loaded: %v", err)
		http.Error(w, "requestExchangeToken cookie is missing or expired - the page could not be loaded", http.StatusInternalServerError)
		return
	}

	debtRequest, err := debtrequestprocessUserCase.AddBSNToRequest(bsn, cookie.Value)
	if err == debtrequestprocess.ErrGettingDebtRequestForRequestId {
		log.Printf("error could not retrieve debt request for request id: %v", err)
		http.Error(w, "error could not retrieve debt request for request id", http.StatusInternalServerError)
		return
	} else if err != nil {
		log.Printf("error failed to add bsn to request: %v", err)
		http.Error(w, "error failed to add bsn to request", http.StatusInternalServerError)
		return
	}

	redirectUrl := fmt.Sprintf("blauweknop.app.login://login?requestExchangeToken=%s", cookie.Value)
	err = tmpl.Execute(w, &callback{
		Name:              organizationName,
		LogoURL:           organizationLogoURL,
		RedirectURL:       template.URL(redirectUrl),
		OrganizationNames: debtRequest.GetOrganizationNames(),
	})

	if err != nil {
		log.Printf("error serving template: %v", err)
		http.Error(w, "the page could not be loaded", http.StatusInternalServerError)
		return
	}
}
