// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package http_test

import (
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"

	"debtrequestprocess"
	http_infra "debtrequestprocess/http"
)

const DummyBSN = "0000000000"

func Test_CreateRouter_Callback(t *testing.T) {
	type fields struct {
		routerConfig http_infra.RouterConfig
	}
	type args struct {
		bsn     string
		cookies []*http.Cookie
	}
	tests := []struct {
		name                   string
		fields                 fields
		args                   args
		expectedHTTPStatusCode int
		expectedBody           string
	}{
		{
			"template file is corrupt / invalid",
			fields{
				http_infra.RouterConfig{
					OrganizationName:      "my-organization",
					LogoURL:               "https://my-organization.com/logo.svg",
					HTMLTemplateDirectory: "../testing/invalid-html-templates",
					CallbackURL:           "",
					LoginServiceURL:       "",
					DebtRequestUseCase: func() *debtrequestprocess.UseCases {
						requestExchangeTokenRepository := generateRequestExchangeTokenRepository(t)
						schemeRepository := generateSchemeRepo(t)
						debtRequestRepository := generateDebtRequestRepository(t)
						tokenGenerator := generateTokenGeneratorRepo(t)

						result := debtrequestprocess.NewUseCases(
							requestExchangeTokenRepository,
							debtRequestRepository,
							schemeRepository,
							generateLoginService(t),
							tokenGenerator,
						)

						return result
					}(),
					GetCurrentTime: mockedGetCurrentTime,
				},
			},
			args{
				bsn: DummyBSN,
			},
			http.StatusInternalServerError,
			"the page could not be loaded\n",
		},
		{
			"template does not exist",
			fields{
				http_infra.RouterConfig{
					OrganizationName:      "my-organization",
					LogoURL:               "https://my-organization.com/logo.svg",
					HTMLTemplateDirectory: "non-existing-template-directory",
					CallbackURL:           "",
					LoginServiceURL:       "",
					DebtRequestUseCase: func() *debtrequestprocess.UseCases {
						requestExchangeTokenRepository := generateRequestExchangeTokenRepository(t)
						schemeRepository := generateSchemeRepo(t)
						debtRequestRepository := generateDebtRequestRepository(t)
						tokenGenerator := generateTokenGeneratorRepo(t)

						result := debtrequestprocess.NewUseCases(
							requestExchangeTokenRepository,
							debtRequestRepository,
							schemeRepository,
							generateLoginService(t),
							tokenGenerator,
						)

						return result
					}(),
					GetCurrentTime: mockedGetCurrentTime,
				},
			},
			args{
				bsn: DummyBSN,
			},
			http.StatusInternalServerError,
			"the page could not be loaded\n",
		},
		{
			"without providing a bsn",
			fields{
				http_infra.RouterConfig{
					OrganizationName:      "my-organization",
					LogoURL:               "https://my-organization.com/logo.svg",
					HTMLTemplateDirectory: "../html/templates",
					DebtRequestUseCase: func() *debtrequestprocess.UseCases {
						requestExchangeTokenRepository := generateRequestExchangeTokenRepository(t)
						schemeRepository := generateSchemeRepo(t)
						debtRequestRepository := generateDebtRequestRepository(t)
						tokenGenerator := generateTokenGeneratorRepo(t)

						result := debtrequestprocess.NewUseCases(
							requestExchangeTokenRepository,
							debtRequestRepository,
							schemeRepository,
							generateLoginService(t),
							tokenGenerator,
						)

						return result
					}(),
					GetCurrentTime: mockedGetCurrentTime,
				},
			},
			args{
				bsn: "",
			},
			400,
			"bsn required\n",
		},
		{
			"requestExchangeToken cookie is missing or expired",
			fields{
				http_infra.RouterConfig{
					OrganizationName:      "my-organization",
					LogoURL:               "https://my-organization.com/logo.svg",
					HTMLTemplateDirectory: "../html/templates",
					DebtRequestUseCase: func() *debtrequestprocess.UseCases {
						requestExchangeTokenRepository := generateRequestExchangeTokenRepository(t)
						requestExchangeTokenRepository.EXPECT().GetRequestIdForRequestExchangeToken("dummy-request-exchange-token").Return("", errors.New("arbitrary error")).AnyTimes()
						schemeRepository := generateSchemeRepo(t)
						debtRequestRepository := generateDebtRequestRepository(t)
						tokenGenerator := generateTokenGeneratorRepo(t)

						result := debtrequestprocess.NewUseCases(
							requestExchangeTokenRepository,
							debtRequestRepository,
							schemeRepository,
							generateLoginService(t),
							tokenGenerator,
						)

						return result
					}(),
					GetCurrentTime: mockedGetCurrentTime,
				},
			},
			args{
				bsn: DummyBSN,
			},
			http.StatusInternalServerError,
			"requestExchangeToken cookie is missing or expired - the page could not be loaded\n",
		},
		{
			"failed to get debt request for a request exchange token",
			fields{
				http_infra.RouterConfig{
					OrganizationName:      "my-organization",
					LogoURL:               "https://my-organization.com/logo.svg",
					HTMLTemplateDirectory: "../html/templates",
					DebtRequestUseCase: func() *debtrequestprocess.UseCases {
						requestExchangeTokenRepository := generateRequestExchangeTokenRepository(t)
						requestExchangeTokenRepository.EXPECT().GetRequestIdForRequestExchangeToken("dummy-request-exchange-token").Return("", errors.New("arbitrary error")).AnyTimes()
						schemeRepository := generateSchemeRepo(t)
						debtRequestRepository := generateDebtRequestRepository(t)
						tokenGenerator := generateTokenGeneratorRepo(t)

						result := debtrequestprocess.NewUseCases(
							requestExchangeTokenRepository,
							debtRequestRepository,
							schemeRepository,
							generateLoginService(t),
							tokenGenerator,
						)

						return result
					}(),
					GetCurrentTime: mockedGetCurrentTime,
				},
			},
			args{
				bsn: DummyBSN,
				cookies: []*http.Cookie{
					{
						Name:  "requestExchangeToken",
						Value: "dummy-request-exchange-token",
					},
				},
			},
			http.StatusInternalServerError,
			"error failed to add bsn to request\n",
		},
		{
			"failed to add bsn to request",
			fields{
				http_infra.RouterConfig{
					OrganizationName:      "my-organization",
					LogoURL:               "https://my-organization.com/logo.svg",
					HTMLTemplateDirectory: "../html/templates",
					DebtRequestUseCase: func() *debtrequestprocess.UseCases {
						requestExchangeTokenRepository := generateRequestExchangeTokenRepository(t)
						requestExchangeTokenRepository.EXPECT().GetRequestIdForRequestExchangeToken("dummy-request-exchange-token").Return("dummy-request-id", nil).AnyTimes()

						schemeRepository := generateSchemeRepo(t)
						tokenGenerator := generateTokenGeneratorRepo(t)

						debtRequestRepository := generateDebtRequestRepository(t)
						debtRequestRepository.EXPECT().Get("dummy-request-id").Return(&debtrequestprocess.DebtRequest{
							Id:  "dummy-request-id",
							BSN: DummyBSN,
							Organizations: []*debtrequestprocess.Organization{{
								OIN:        "mock-oin",
								Name:       "name",
								APIBaseURL: "apiBaseUrl",
								LoginURL:   "loginUrl",
							},
							},
						}, nil).AnyTimes()
						debtRequestRepository.EXPECT().Update("dummy-request-id", debtrequestprocess.DebtRequest{
							Id:  "dummy-request-id",
							BSN: DummyBSN,
							Organizations: []*debtrequestprocess.Organization{{
								OIN:        "mock-oin",
								Name:       "name",
								APIBaseURL: "apiBaseUrl",
								LoginURL:   "loginUrl",
							},
							},
						}).Return(errors.New("arbitrary error")).AnyTimes()

						result := debtrequestprocess.NewUseCases(
							requestExchangeTokenRepository,
							debtRequestRepository,
							schemeRepository,
							generateLoginService(t),
							tokenGenerator,
						)

						return result
					}(),
					GetCurrentTime: mockedGetCurrentTime,
				},
			},
			args{
				bsn: DummyBSN,
				cookies: []*http.Cookie{
					{
						Name:  "requestExchangeToken",
						Value: "dummy-request-exchange-token",
					},
				},
			},
			http.StatusInternalServerError,
			"error failed to add bsn to request\n",
		},
		{
			"happy flow",
			fields{
				http_infra.RouterConfig{
					OrganizationName:      "my-organization",
					LogoURL:               "https://my-organization.com/logo.svg",
					HTMLTemplateDirectory: "../html/templates",
					DebtRequestUseCase: func() *debtrequestprocess.UseCases {
						schemeRepository := generateSchemeRepo(t)

						requestExchangeTokenRepository := generateRequestExchangeTokenRepository(t)
						requestExchangeTokenRepository.EXPECT().GetRequestIdForRequestExchangeToken("dummy-request-exchange-token").Return("dummy-request-id", nil).AnyTimes()

						debtRequestRepository := generateDebtRequestRepository(t)
						debtRequestRepository.EXPECT().Get("dummy-request-id").Return(&debtrequestprocess.DebtRequest{
							Id: "dummy-request-id",
							Organizations: []*debtrequestprocess.Organization{{
								OIN:        "mock-oin",
								Name:       "name",
								APIBaseURL: "apiBaseUrl",
								LoginURL:   "loginUrl",
							},
							},
						}, nil).AnyTimes()
						debtRequestRepository.EXPECT().Update("dummy-request-id", debtrequestprocess.DebtRequest{
							Id:  "dummy-request-id",
							BSN: DummyBSN,
							Organizations: []*debtrequestprocess.Organization{{
								OIN:        "mock-oin",
								Name:       "name",
								APIBaseURL: "apiBaseUrl",
								LoginURL:   "loginUrl",
							},
							},
						}).Return(nil).MaxTimes(1)

						tokenGenerator := generateTokenGeneratorRepo(t)

						result := debtrequestprocess.NewUseCases(
							requestExchangeTokenRepository,
							debtRequestRepository,
							schemeRepository,
							generateLoginService(t),
							tokenGenerator,
						)

						return result
					}(),
					GetCurrentTime: mockedGetCurrentTime,
				},
			},
			args{
				bsn: DummyBSN,
				cookies: []*http.Cookie{
					{
						Name:  "requestExchangeToken",
						Value: "dummy-request-exchange-token",
					},
				},
			},
			http.StatusOK,
			"<!DOCTYPE html>\n<html>\n\n<head>\n    <meta charset=\"utf-8\" />\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\" />\n    <title>Login</title>\n\n    <link href=\"https://fonts.googleapis.com/css?family=Open+Sans:400,600\" rel=\"stylesheet\" type=\"text/css\">\n\n    <style>\n        html,\n        body {\n            font-size: 16px;\n            line-height: 1.5rem;\n            font-family: 'Open Sans', sans-serif;\n            color: #212121;\n            font-weight: 400;\n            padding: 0;\n            margin: 0;\n        }\n\n        .container {\n            padding: 0 1rem 1rem 1rem;\n        }\n\n        h1 {\n            font-size: 1.125rem;\n            font-weight: 600;\n        }\n\n        p {\n            font-size: 1rem;\n        }\n\n        button, a.external {\n            text-decoration: none;\n            display: block;\n            cursor: pointer;\n            border: 0 none;\n            border-radius: 5px;\n            color: #ffffff;\n            text-align: center;\n            line-height: 2.75rem;\n            width: 100%;\n            max-width: 100%;\n            font-size: 1rem;\n            font-family: 'Open Sans', sans-serif;\n        }\n\n        button[type=\"submit\"] {\n            background-color: #063D63;\n            background-repeat: no-repeat;\n            background-size: 2.2rem;\n            background-position: 5px;\n        }\n\n        a.external {\n            background-color: #063D63;\n            background-image: url('data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyNCIgaGVpZ2h0PSIyNCI+PHBhdGggZmlsbD0iI2ZmZmZmZiIgZD0iTTUgM2MtMS4wOTMgMC0yIC45MDctMiAydjE0YzAgMS4wOTMuOTA3IDIgMiAyaDE0YzEuMDkzIDAgMi0uOTA3IDItMnYtN2gtMnY3SDVWNWg3VjNINXptOSAwdjJoMy41ODZsLTkuMjkzIDkuMjkzIDEuNDE0IDEuNDE0TDE5IDYuNDE0VjEwaDJWM2gtN3oiLz48L3N2Zz4=');\n            background-repeat: no-repeat;\n            background-position: right 10px center;\n        }\n\n        .hidden {\n            display: none;\n        }\n\n        div.check {\n            display: block;\n            width: 100%;\n            height: 64px;\n            background-image: url('data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iNDQiIGhlaWdodD0iNDQiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGcgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIj48Y2lyY2xlIGZpbGw9IiMzOTg3MEMiIGN4PSIyMiIgY3k9IjIyIiByPSIyMiIvPjxwYXRoIGQ9Ik0xOSAyOGwxMi45OTktMTNMMzQgMTcgMTkgMzJsLTktOSAyLTJ6IiBmaWxsPSIjRkZGIiBmaWxsLXJ1bGU9Im5vbnplcm8iLz48L2c+PC9zdmc+');\n            background-position: center;\n            background-repeat: no-repeat;\n        }\n\n        label {\n            display: flex;\n            align-items: self-start;\n            margin-bottom: 1rem;\n        }\n\n        label input {\n            flex: 0 0 25px;\n        }\n\n        label span {\n            flex: 1;\n        }\n\n        .collapsible-organizations {\n            position: relative;\n            border-top: 1px solid #E0E0E0;\n            border-bottom: 1px solid #E0E0E0;\n            padding: .75rem 0;\n            margin-bottom: 2rem;\n        }\n\n        .collapsible-organizations a {\n            text-decoration: none;\n            color: #212121;\n            display: block;\n        }\n\n        .collapsible-organizations ul {\n            display: none;\n        }\n\n        .collapsible-organizations::before {\n            content: ' ';\n            position: absolute;\n            display: block;\n            width: 24px;\n            height: 24px;\n            background-image: url('data:image/svg+xml;base64,PHN2ZyB2aWV3Qm94PSIwIDAgMjQgMjQiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgZmlsbC1ydWxlPSJldmVub2RkIiBjbGlwLXJ1bGU9ImV2ZW5vZGQiIHN0cm9rZS1saW5lam9pbj0icm91bmQiIHN0cm9rZS1taXRlcmxpbWl0PSIyIj48cGF0aCBkPSJNMTIgMTAuOTM5bC00Ljk1IDQuOTUtMS40MTQtMS40MTRMMTIgOC4xMTFsNi4zNjQgNi4zNjQtMS40MTQgMS40MTQtNC45NS00Ljk1eiIgZmlsbC1ydWxlPSJub256ZXJvIi8+PC9zdmc+');\n            background-position: center;\n            background-repeat: no-repeat;\n            fill: #6a6a6a;\n            transform: rotate(180deg);\n            top: .75rem;\n            right: 0;\n            transition: 150ms ease-in-out;\n        }\n\n        .collapsible-organizations.is-open::before {\n            transform: rotate(0deg);\n        }\n\n        .collapsible-organizations.is-open ul {\n            display: block;\n        }\n\n        .hidden {\n            display: none;\n        }\n    </style>\n</head>\n\n<body>\n\n    <img src=\"https://my-organization.com/logo.svg\" style=\"max-height: 75px;max-width: 100%;\">\n    <div class=\"container\">\n        <div id=\"confirm-page\">\n            <h1>Bevestig uw verzoek</h1>\n\n            <p>Uw doet een verzoek tot het ophalen van schuldinformatie bij: </p>\n\n            <div class=\"collapsible-organizations\">\n                <a href=\"#\">1 overheidsorganisaties</a>\n                <ul>\n                    \n                        <li>name</li>\n                    \n                </ul>\n            </div>\n\n            <form>\n                <label>\n                    <input type=\"checkbox\"> <span>Hierbij geef ik Gemeente Tilburg toestemming mijn verzoek om schuldgegevens beschikbaar te stellen aan 1 overheidsorganisaties.</span>\n                </label>\n\n                <button type=\"submit\">Verzoek bevestigen</button>\n            </form>\n        </div>\n\n        <div id=\"success-page\" class=\"hidden\">\n            <div class=\"check\"></div>\n\n            <h1 style=\"text-align: center;\">Verzoek bevestigd</h1>\n\n            <p>Uw verzoek is ontvangen en wordt aan 1 organisaties beschikbaar gesteld.</p>\n            <a href=blauweknop.app.login://login?requestExchangeToken&#61;dummy-request-exchange-token class=\"external\">Ga verder in de Blauwe Knop app</a>\n        </div>\n\n    </div>\n\n    <script>\n        var form = document.querySelector('form')\n        var checkbox = document.querySelector('input[type=\"checkbox\"]')\n\n        form.addEventListener('submit', (event) => {\n            event.preventDefault()\n\n          if (!checkbox.checked) {\n            return;\n          }\n\n            document.getElementById('confirm-page').classList.toggle('hidden')\n            document.getElementById('success-page').classList.toggle('hidden')\n        })\n\n        var collapsibleOrganizationsElement = document.querySelector('.collapsible-organizations')\n        var collapsibleOrganizationsLinkElement = collapsibleOrganizationsElement.querySelector('a')\n        collapsibleOrganizationsLinkElement.addEventListener('click', function (event) {\n          event.preventDefault()\n          collapsibleOrganizationsElement.classList.toggle('is-open')\n        })\n    </script>\n</body>\n\n</html>\n",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			router, err := http_infra.NewRouter(test.fields.routerConfig)
			assert.Nil(t, err)
			w := httptest.NewRecorder()

			request := httptest.NewRequest("GET", fmt.Sprintf("/debt-request-process/callback?bsn=%s", test.args.bsn), nil)
			for _, cookie := range test.args.cookies {
				request.AddCookie(cookie)
			}
			router.ServeHTTP(w, request)

			resp := w.Result()
			body, _ := ioutil.ReadAll(resp.Body)

			assert.Equal(t, test.expectedHTTPStatusCode, resp.StatusCode)
			assert.Equal(t, test.expectedBody, string(body))
		})
	}
}
