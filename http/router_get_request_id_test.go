// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package http_test

import (
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"debtrequestprocess"
	http_infra "debtrequestprocess/http"
)

const DummyRequestExchangeToken = "dummy-request-exchange-token"

func Test_CreateRouter_GetRequestId(t *testing.T) {
	type fields struct {
		routerConfig http_infra.RouterConfig
	}
	type args struct {
		requestExchangeToken string
	}
	tests := []struct {
		name                   string
		fields                 fields
		args                   args
		expectedHTTPStatusCode int
		expectedBody           string
	}{
		{
			"without providing a request exchange token",
			fields{
				http_infra.RouterConfig{
					OrganizationName:      "my-organization",
					LogoURL:               "https://my-organization.com/logo.svg",
					HTMLTemplateDirectory: "../html/templates",
					DebtRequestUseCase: func() *debtrequestprocess.UseCases {
						requestExchangeTokenRepository := generateRequestExchangeTokenRepository(t)
						schemeRepository := generateSchemeRepo(t)
						debtRequestRepository := generateDebtRequestRepository(t)
						tokenGenerator := generateTokenGeneratorRepo(t)

						result := debtrequestprocess.NewUseCases(
							requestExchangeTokenRepository,
							debtRequestRepository,
							schemeRepository,
							generateLoginService(t),
							tokenGenerator,
						)

						return result
					}(),
					GetCurrentTime: mockedGetCurrentTime,
				},
			},
			args{
				requestExchangeToken: "",
			},
			http.StatusBadRequest,
			"requestExchangeToken required\n",
		},
		{
			"with an invalid request exchange token",
			fields{
				http_infra.RouterConfig{
					OrganizationName:      "my-organization",
					LogoURL:               "https://my-organization.com/logo.svg",
					HTMLTemplateDirectory: "../html/templates",
					DebtRequestUseCase: func() *debtrequestprocess.UseCases {
						requestExchangeTokenRepository := generateRequestExchangeTokenRepository(t)
						requestExchangeTokenRepository.EXPECT().GetRequestIdForRequestExchangeToken(gomock.Any()).Return("", nil).AnyTimes()
						schemeRepository := generateSchemeRepo(t)
						debtRequestRepository := generateDebtRequestRepository(t)
						tokenGenerator := generateTokenGeneratorRepo(t)

						result := debtrequestprocess.NewUseCases(
							requestExchangeTokenRepository,
							debtRequestRepository,
							schemeRepository,
							generateLoginService(t),
							tokenGenerator,
						)

						return result
					}(),
					GetCurrentTime: mockedGetCurrentTime,
				},
			},
			args{
				requestExchangeToken: DummyRequestExchangeToken,
			},
			http.StatusBadRequest,
			"request exchange token is invalid\n",
		},
		{
			"failed to get request for the request exchange token",
			fields{
				http_infra.RouterConfig{
					OrganizationName:      "my-organization",
					LogoURL:               "https://my-organization.com/logo.svg",
					HTMLTemplateDirectory: "../html/templates",
					DebtRequestUseCase: func() *debtrequestprocess.UseCases {
						requestExchangeTokenRepository := generateRequestExchangeTokenRepository(t)
						requestExchangeTokenRepository.EXPECT().GetRequestIdForRequestExchangeToken(gomock.Any()).Return("", errors.New("arbitrary error")).AnyTimes()
						schemeRepository := generateSchemeRepo(t)
						debtRequestRepository := generateDebtRequestRepository(t)
						tokenGenerator := generateTokenGeneratorRepo(t)

						result := debtrequestprocess.NewUseCases(
							requestExchangeTokenRepository,
							debtRequestRepository,
							schemeRepository,
							generateLoginService(t),
							tokenGenerator,
						)

						return result
					}(),
					GetCurrentTime: mockedGetCurrentTime,
				},
			},
			args{
				requestExchangeToken: DummyBSN,
			},
			http.StatusInternalServerError,
			"error exchanging token\n",
		},
		{
			"happy flow",
			fields{
				http_infra.RouterConfig{
					OrganizationName:      "my-organization",
					LogoURL:               "https://my-organization.com/logo.svg",
					HTMLTemplateDirectory: "../html/templates",
					DebtRequestUseCase: func() *debtrequestprocess.UseCases {
						requestExchangeTokenRepository := generateRequestExchangeTokenRepository(t)
						requestExchangeTokenRepository.EXPECT().GetRequestIdForRequestExchangeToken("dummy-request-exchange-token").Return("dummy-request-id", nil).AnyTimes()
						schemeRepository := generateSchemeRepo(t)
						debtRequestRepository := generateDebtRequestRepository(t)
						debtRequestRepository.EXPECT().Get("dummy-request-id").Return(&debtrequestprocess.DebtRequest{
							Id: "dummy-request-id",
							Organizations: []*debtrequestprocess.Organization{{
								OIN:        "mock-oin",
								Name:       "name",
								APIBaseURL: "apiBaseUrl",
								LoginURL:   "loginUrl",
							},
							},
						}, nil).AnyTimes()

						tokenGenerator := generateTokenGeneratorRepo(t)

						result := debtrequestprocess.NewUseCases(
							requestExchangeTokenRepository,
							debtRequestRepository,
							schemeRepository,
							generateLoginService(t),
							tokenGenerator,
						)

						return result
					}(),
					GetCurrentTime: mockedGetCurrentTime,
				},
			},
			args{
				requestExchangeToken: DummyRequestExchangeToken,
			},
			http.StatusOK,
			"dummy-request-id",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			router, err := http_infra.NewRouter(test.fields.routerConfig)
			assert.Nil(t, err)
			w := httptest.NewRecorder()

			request := httptest.NewRequest("GET", fmt.Sprintf("/debt-request-process/exchangetoken?requestExchangeToken=%s", test.args.requestExchangeToken), nil)
			router.ServeHTTP(w, request)

			resp := w.Result()
			body, _ := ioutil.ReadAll(resp.Body)

			assert.Equal(t, test.expectedHTTPStatusCode, resp.StatusCode)
			assert.Equal(t, test.expectedBody, string(body))
		})
	}
}
