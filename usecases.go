// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package debtrequestprocess

import (
	healthcheck "gitlab.com/commonground/blauwe-knop/health-checker/healthchecker"
)

type SchemeRepository interface {
	GetOrganizationByOIN(OIN string) (*Organization, error)
	healthcheck.Checker
}

type DebtRequestRepository interface {
	Create(request DebtRequest) (*DebtRequest, error)
	Update(id string, request DebtRequest) error
	Get(id string) (*DebtRequest, error)
	healthcheck.Checker
}

type RequestExchangeTokenRepository interface {
	AddRequestExchangeToken(requestId, requestExchangeToken string) error
	DeleteRequestExchangeToken(requestExchangeToken string) error
	GetRequestIdForRequestExchangeToken(requestExchangeToken string) (requestId string, error error)
	healthcheck.Checker
}

type LoginService interface {
	healthcheck.Checker
}

type TokenGenerator interface {
	GenerateRequestExchangeToken() string
}

type UseCases struct {
	schemeRepository               SchemeRepository
	debtRequestRepository          DebtRequestRepository
	requestExchangeTokenRepository RequestExchangeTokenRepository
	loginService                   LoginService
	tokenGenerator                 TokenGenerator
}

func NewUseCases(
	requestExchangeTokenRepository RequestExchangeTokenRepository,
	debtRequestRepository DebtRequestRepository,
	repository SchemeRepository,
	loginService LoginService,
	tokenGenerator TokenGenerator,
) *UseCases {
	return &UseCases{
		repository,
		debtRequestRepository,
		requestExchangeTokenRepository,
		loginService,
		tokenGenerator,
	}
}

func (a *UseCases) GetHealthChecks() []healthcheck.Checker {
	return []healthcheck.Checker{
		a.schemeRepository,
		a.debtRequestRepository,
		a.requestExchangeTokenRepository,
		a.loginService,
	}
}
