// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package debtrequestprocess_test

import (
	"testing"

	"github.com/golang/mock/gomock"

	"debtrequestprocess/mock"
)

func generateRequestExchangeTokenRepository(t *testing.T) *mock.MockRequestExchangeTokenRepository {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	repo := mock.NewMockRequestExchangeTokenRepository(ctrl)
	return repo
}

func generateDebtRequestRepository(t *testing.T) *mock.MockDebtRequestRepository {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	repo := mock.NewMockDebtRequestRepository(ctrl)
	return repo
}

func generateSchemeRepo(t *testing.T) *mock.MockSchemeRepository {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	repo := mock.NewMockSchemeRepository(ctrl)
	return repo
}

var dummyPublicKeyPem string = `-----BEGIN PUBLIC KEY-----
MIICCgKCAgEAtmmuZZzzSx4OROsWwDvuKPOD6gQgEYhlGZvuJzk1sTKL8baOaJRH5feQYtX5wH4PYtr/GjJ5R9alaRt2l4/ROIx7/zRY6KVSYb8QH6gc5t3pYfRA76SZKS5gKbZP7wqCjc8VXdc0VurJuq7qlZyajc/qdYnknilWy0fRQ05SvB2pZ4BbwYBDKaJD4DUdkijV9p4D92WhwwlY2r2ZOKBFZD/EYn7kU1MMe4uNd8Y970AieGfe//Q2GGogg+lzZDdNVeqCCCbmezGqu6tp6lyeyDpwYNvXUccL4FZFIPw5WRmhbV5gI020hEPHanyJ991avS11CfbYmfnv1fDPOL+9wEtPU+KXVMg7LFfzHc3ZYvO+HnhinF2LOID2AP+N5Kn34H+vWb82lqqXz63w2Je3Dznp3G1oZAPnHHTPDwv0eD3fYyBp6lhFtAlrReGPQEL0h34HbWxEKw39uExGVvWEnx73uD2pKQLQqG8+DqY5PdtooG2/tBQEUhBKgs/U9iM2J2g7BaRM4IlssJa+ByqexzFggDjGq1+F8833LoE3v34xFI/3LekuMTF4pv51XFCLUp3uhwP8hJQ9PTsxKm13wLsiws63BGXShB/dP0ySxyhTEINqC/LBqDbX6P3kfd0aJKB0CAflXQTzdIFu/JJRDnxqFzpey+lAq/U++mVlNb0CAwEAAQ==
-----END PUBLIC KEY-----`
