// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package main

import (
	"log"
	"net/http"
	"net/url"
	"time"

	"github.com/svent/go-flags"

	"debtrequestprocess"
	file_infra "debtrequestprocess/file"
	http_infra "debtrequestprocess/http"
	"debtrequestprocess/redis"
)

var options struct {
	Organization               string `long:"organization" env:"ORGANIZATION" description:"Organization name"`
	OrganizationLogoURL        string `long:"organization-logo-url" env:"ORGANIZATION_LOGO_URL" description:"Organization logo URL"`
	LoginServiceURL            string `long:"login-service-url" env:"LOGIN_SERVICE_URL" description:"URL of the login service"`
	CallbackURL                string `long:"callback-url" env:"CALLBACK_URL" description:"callback url provided to login service URL"`
	SchemeURI                  string `long:"scheme-uri" env:"SCHEME_URI" default:"https://gitlab.com/commonground/blauwe-knop/scheme/-/raw/master/organizations.json" description:"URI of the scheme containing the organizations. Can be a file path or a URL"`
	HtmlTemplateDirectory      string `long:"html-template-directory" env:"HTML_TEMPLATE_DIRECTORY" default:"html/templates/" description:"The directory in which the html templates can be found"`
	ListenAddress              string `long:"listen-address" env:"LISTEN_ADDRESS" default:"0.0.0.0:8083" description:"Address for the debtrequestprocess to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs."`
	RedisDSN                   string `long:"redis-dsn" env:"REDIS_DSN" default:"redis://0.0.0.0:6379/0" description:"DSN for the redis database. Read https://pkg.go.dev/github.com/go-redis/redis/v8?tab=doc#ParseURL for more info"`
	DebtRequestRegisterAddress string `long:"debt-request-register-address" env:"DEBT_REQUEST_REGISTER_ADDRESS" default:"http://localhost:8086" description:"Debt request register address."`
	DebtRequestRegisterAPIKey  string `long:"debt-request-register-api-key" env:"DEBT_REQUEST_REGISTER_API_KEY" default:"" description:"API key to use when calling the debt-request-register service."`
}

func main() {
	// Parse options
	args, err := flags.Parse(&options)
	if err != nil {
		if et, ok := err.(*flags.Error); ok {
			if et.Type == flags.ErrHelp {
				return
			}
		}
		log.Fatalf("error parsing flags: %v", err)
	}
	if len(args) > 0 {
		log.Fatalf("unexpected arguments: %v", args)
	}

	if options.Organization == "" {
		log.Fatalf("please specify an organization")
	}

	if options.LoginServiceURL == "" {
		log.Fatalf("please specify an login service URL")
	}

	if options.CallbackURL == "" {
		log.Fatalf("please specify an callback URL")
	}

	requestExchangeTokenRepository, err := redis.NewRequestExchangeTokenRepository(options.RedisDSN)
	if err != nil {
		panic(err)
	}

	debtRequestRegisterRepository := http_infra.NewDebtRequestRegisterRepository(options.DebtRequestRegisterAddress, options.DebtRequestRegisterAPIKey)

	url, err := url.Parse(options.SchemeURI)
	if err != nil {
		log.Fatalf("error parsing scheme URI %v", err)
	}

	var schemeRepository debtrequestprocess.SchemeRepository
	if url.Scheme == "file" {
		schemeRepository, err = file_infra.NewSchemeRepository(url.Hostname())
		if err != nil {
			log.Fatalf("error create file scheme repository: %v", err)
		}
	} else {
		schemeRepository = http_infra.NewSchemeRepository(options.SchemeURI)
	}

	loginService := http_infra.NewLoginService(options.LoginServiceURL)

	useCase := debtrequestprocess.NewUseCases(requestExchangeTokenRepository, debtRequestRegisterRepository, schemeRepository, loginService, debtrequestprocess.NewRandomTokenGenerator())
	routerConfig := http_infra.RouterConfig{
		LogoURL:               options.OrganizationLogoURL,
		OrganizationName:      options.Organization,
		HTMLTemplateDirectory: options.HtmlTemplateDirectory,
		CallbackURL:           options.CallbackURL,
		LoginServiceURL:       options.LoginServiceURL,
		DebtRequestUseCase:    useCase,
		GetCurrentTime: func() time.Time {
			return time.Now()
		},
	}
	router, err := http_infra.NewRouter(routerConfig)
	if err != nil {
		log.Fatalf("unable to start router: %v", err)
	}

	log.Printf("start listening on  %s", options.ListenAddress)
	err = http.ListenAndServe(options.ListenAddress, router)
	if err != nil {
		if err != http.ErrServerClosed {
			panic(err)
		}
	}
}
