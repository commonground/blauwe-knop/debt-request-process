# Debt Request Process

The Dept Request Process written in Golang. The application facilitates collecting debt information from multiple source organizations

## Installation

Prerequisites:

- [Git](https://git-scm.com/)
- [Golang](https://golang.org/doc/install)
- [Modd](https://github.com/cortesi/modd)
- [Redis](https://redis.io)

1. Download the required Go dependencies:

```sh
go mod download
```

1. Spin up Redis. We've provided instructions on how to do this using Docker Compose below.

1. Now start the application:

```sh
go run cmd/debt-request-process/main.go
    --organization demo-org
    --organization-logo-url https://assets.gitlab-static.net/uploads/-/system/group/avatar/7745679/BEELDMERK_NEW.png?width=64
    --login-service-url {your login service}
    --callback-url http://localhost:8083/debt-request-process/callback
    --organization-logo-url https://assets.gitlab-static.net/uploads/-/system/group/avatar/7745679/BEELDMERK_NEW.png?width=64
```

Or run the application using modd, which wil restart the API on file changes.

```sh
modd
```

By default, the debt request process will run on port `8083`.

### Running Redis

The authorization service uses a Redis database for data storage. You can launch it using Docker Compose.

```sh
cd ../blauwe-knop && docker-compose -f docker-compose.dev.yaml up -d
```

> The `-d` flag makes it run in the background. To stop the services simply type: `docker-compose stop`

## Adding mocks

We use [GoMock](https://github.com/golang/mock) to generate mocks.
When you make updates to code for which there are mocks, you should regenerate the mocks.

**Regenerating mocks**

```sh
sh regenerate-gomock-files.sh
```

## Deployment

Prerequisites:

- [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
- [helm](https://helm.sh/docs/intro/)
- deployment access to the haven cluster `azure-common-prod`

You will need to have a [Redis](https://redis.io/) database running on the cluster.
If you do not have Redis running on the cluster you can use `helm` to install it.

First add the chart to `helm`

```sh
helm repo add bitnami https://charts.bitnami.com/bitnami
```

Now use `helm` to deploy the Redis chart on the cluster

```sh
helm install -n bk-test redis bitnami/redis
```

Once Redis is running you can use `helm` to deploy the debt request process.

```sh
helm upgrade --install debt-request-process ./charts/debt-request-process -n bk-test
```
