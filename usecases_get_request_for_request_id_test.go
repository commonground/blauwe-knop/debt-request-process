// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package debtrequestprocess_test

import (
	"errors"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"debtrequestprocess"
	"debtrequestprocess/mock"
)

const DummyRequestID = "arbitrary-request-exchange-token"

func generateDefaultTokenRepo(t *testing.T) *mock.MockTokenGenerator {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	repo := mock.NewMockTokenGenerator(ctrl)
	return repo
}

func generateLoginService(t *testing.T) *mock.MockLoginService {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	repo := mock.NewMockLoginService(ctrl)
	return repo
}

func Test_UseCase_GetRequestForRequestID(t *testing.T) {
	type fields struct {
		schemeRepository               debtrequestprocess.SchemeRepository
		requestExchangeTokenRepository debtrequestprocess.RequestExchangeTokenRepository
		debtRequestRepository          debtrequestprocess.DebtRequestRepository
		tokenGenerator                 debtrequestprocess.TokenGenerator
	}
	type args struct {
		requestID string
	}
	tests := []struct {
		name          string
		fields        fields
		args          args
		want          *debtrequestprocess.DebtRequest
		expectedError error
	}{
		{
			name: "for a non existing request ID",
			fields: fields{
				schemeRepository:               generateSchemeRepo(t),
				requestExchangeTokenRepository: generateRequestExchangeTokenRepository(t),
				debtRequestRepository: func() *mock.MockDebtRequestRepository {
					repo := generateDebtRequestRepository(t)
					repo.EXPECT().Get(gomock.Any()).Return(nil, nil).AnyTimes()
					return repo
				}(),
				tokenGenerator: generateDefaultTokenRepo(t),
			},
			args: args{
				"non-existing-request-id",
			},
			want:          nil,
			expectedError: debtrequestprocess.ErrRequestDoesNotExist,
		},
		{
			name: "failed to get the request by ID",
			fields: fields{
				schemeRepository:               generateSchemeRepo(t),
				requestExchangeTokenRepository: generateRequestExchangeTokenRepository(t),
				debtRequestRepository: func() *mock.MockDebtRequestRepository {
					repo := generateDebtRequestRepository(t)
					repo.EXPECT().Get(gomock.Any()).Return(nil, errors.New("arbitrary error")).AnyTimes()
					return repo
				}(),
				tokenGenerator: generateDefaultTokenRepo(t),
			},
			args: args{
				"arbitrary-request-exchange-token",
			},
			want:          nil,
			expectedError: errors.New("failed to get debt request by id: arbitrary error"),
		},
		{
			name: "happy flow",
			fields: fields{
				schemeRepository:               generateSchemeRepo(t),
				requestExchangeTokenRepository: generateRequestExchangeTokenRepository(t),
				debtRequestRepository: func() *mock.MockDebtRequestRepository {
					repo := generateDebtRequestRepository(t)
					repo.EXPECT().Get(DummyRequestID).Return(&debtrequestprocess.DebtRequest{
						Id:            "request-id",
						BSN:           "dummy-bsn",
						Organizations: []*debtrequestprocess.Organization{},
						PublicKeyPEM:  dummyPublicKeyPem,
					}, nil).AnyTimes()
					return repo
				}(),
				tokenGenerator: generateDefaultTokenRepo(t),
			},
			args: args{
				DummyRequestID,
			},
			want: &debtrequestprocess.DebtRequest{
				Id:            "request-id",
				BSN:           "dummy-bsn",
				Organizations: []*debtrequestprocess.Organization{},
				PublicKeyPEM:  dummyPublicKeyPem,
			},
			expectedError: nil,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := debtrequestprocess.NewUseCases(
				tt.fields.requestExchangeTokenRepository,
				tt.fields.debtRequestRepository,
				tt.fields.schemeRepository,
				generateLoginService(t),
				tt.fields.tokenGenerator,
			)
			got, err := d.GetDebtRequestForDebtRequestID(tt.args.requestID)
			if tt.expectedError != nil {
				assert.Error(t, err, tt.expectedError.Error())
			} else {
				assert.Nil(t, err)
			}

			assert.Equal(t, tt.want, got)
		})
	}
}
