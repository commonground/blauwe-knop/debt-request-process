package debtrequestprocess

type Organization struct {
	OIN        string
	Name       string
	APIBaseURL string
	LoginURL   string
}
