// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package debtrequestprocess_test

import (
	"errors"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"debtrequestprocess"
	"debtrequestprocess/mock"
)

func Test_UseCase_GetRequestForRequestExchangeToken(t *testing.T) {
	type fields struct {
		schemeRepository               debtrequestprocess.SchemeRepository
		requestExchangeTokenRepository debtrequestprocess.RequestExchangeTokenRepository
		debtRequestRepository          debtrequestprocess.DebtRequestRepository
		tokenGenerator                 debtrequestprocess.TokenGenerator
	}
	type args struct {
		requestExchangeToken string
	}
	tests := []struct {
		name          string
		fields        fields
		args          args
		want          *debtrequestprocess.DebtRequest
		expectedError error
	}{
		{
			name: "for a non existing request token",
			fields: fields{
				schemeRepository: func() debtrequestprocess.SchemeRepository {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					repo := mock.NewMockSchemeRepository(ctrl)
					return repo
				}(),
				debtRequestRepository: func() debtrequestprocess.DebtRequestRepository {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					repo := mock.NewMockDebtRequestRepository(ctrl)
					return repo
				}(),
				requestExchangeTokenRepository: func() debtrequestprocess.RequestExchangeTokenRepository {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					repo := mock.NewMockRequestExchangeTokenRepository(ctrl)
					repo.EXPECT().GetRequestIdForRequestExchangeToken("non-existing-request-exchange-token").Return("", nil).MaxTimes(1)
					return repo
				}(),
				tokenGenerator: func() debtrequestprocess.TokenGenerator {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					repo := mock.NewMockTokenGenerator(ctrl)
					return repo
				}(),
			},
			args: args{
				"non-existing-request-exchange-token",
			},
			want:          nil,
			expectedError: debtrequestprocess.ErrRequestExchangeTokenInvalid,
		},
		{
			name: "failed to get the request exchange token request",
			fields: fields{
				schemeRepository: func() debtrequestprocess.SchemeRepository {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					repo := mock.NewMockSchemeRepository(ctrl)
					return repo
				}(),
				debtRequestRepository: func() debtrequestprocess.DebtRequestRepository {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					repo := mock.NewMockDebtRequestRepository(ctrl)
					return repo
				}(),
				requestExchangeTokenRepository: func() debtrequestprocess.RequestExchangeTokenRepository {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					repo := mock.NewMockRequestExchangeTokenRepository(ctrl)
					repo.EXPECT().GetRequestIdForRequestExchangeToken(gomock.Any()).Return("", errors.New("arbitrary error")).MaxTimes(1)
					return repo
				}(),
				tokenGenerator: func() debtrequestprocess.TokenGenerator {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					repo := mock.NewMockTokenGenerator(ctrl)
					return repo
				}(),
			},
			args: args{
				"arbitrary-request-exchange-token",
			},
			want:          nil,
			expectedError: errors.New("failed to get request id: arbitrary error"),
		},
		{
			name: "happy flow",
			fields: fields{
				schemeRepository: func() debtrequestprocess.SchemeRepository {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					repo := mock.NewMockSchemeRepository(ctrl)
					return repo
				}(),
				requestExchangeTokenRepository: func() debtrequestprocess.RequestExchangeTokenRepository {
					repo := generateRequestExchangeTokenRepository(t)
					repo.EXPECT().GetRequestIdForRequestExchangeToken(gomock.Any()).Return("request-id", nil).MaxTimes(1)
					return repo
				}(),
				debtRequestRepository: func() debtrequestprocess.DebtRequestRepository {
					repo := generateDebtRequestRepository(t)

					repo.EXPECT().Get("request-id").Return(&debtrequestprocess.DebtRequest{
						Id:            "request-id",
						BSN:           "dummy-bsn",
						Organizations: []*debtrequestprocess.Organization{},
					}, nil).MaxTimes(1)
					return repo
				}(),
				tokenGenerator: func() debtrequestprocess.TokenGenerator {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					repo := mock.NewMockTokenGenerator(ctrl)
					return repo
				}(),
			},
			args: args{
				"arbitrary-request-exchange-token",
			},
			want: &debtrequestprocess.DebtRequest{
				Id:            "request-id",
				BSN:           "dummy-bsn",
				Organizations: []*debtrequestprocess.Organization{},
			},
			expectedError: nil,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := debtrequestprocess.NewUseCases(
				tt.fields.requestExchangeTokenRepository,
				tt.fields.debtRequestRepository,
				tt.fields.schemeRepository,
				generateLoginService(t),
				tt.fields.tokenGenerator,
			)
			got, err := d.GetDebtRequest(tt.args.requestExchangeToken)
			if tt.expectedError != nil {
				assert.Error(t, err, tt.expectedError.Error())
			} else {
				assert.Nil(t, err)
			}

			assert.Equal(t, tt.want, got)
		})
	}
}
