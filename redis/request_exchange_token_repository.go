// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package redis

import (
	"fmt"
	"log"
	"math/rand"
	"time"

	"github.com/go-redis/redis/v7"
	healthcheck "gitlab.com/commonground/blauwe-knop/health-checker/healthchecker"
)

type RequestExchangeTokenRepository struct {
	client *redis.Client
}

func NewRequestExchangeTokenRepository(redisDSN string) (*RequestExchangeTokenRepository, error) {
	options, err := redis.ParseURL(redisDSN)
	if err != nil {
		return nil, err
	}

	client := redis.NewClient(options)

	_, err = client.Ping().Result()
	if err != nil {
		return nil, fmt.Errorf("ping to redis failed: %v", err)
	}
	rand.Seed(time.Now().UnixNano())
	return &RequestExchangeTokenRepository{
		client: client,
	}, nil
}

func (r *RequestExchangeTokenRepository) AddRequestExchangeToken(requestId, requestExchangeToken string) error {
	_, err := r.client.Set(fmt.Sprintf("request-exchange-token-%s", requestExchangeToken), requestId, 0).Result()
	return err
}

func (r *RequestExchangeTokenRepository) DeleteRequestExchangeToken(requestExchangeToken string) error {
	_, err := r.client.Del(fmt.Sprintf("request-exchange-token-%s", requestExchangeToken)).Result()
	return err
}

func (r *RequestExchangeTokenRepository) GetRequestIdForRequestExchangeToken(requestExchangeToken string) (string, error) {
	value, err := r.client.Get(fmt.Sprintf("request-exchange-token-%s", requestExchangeToken)).Result()
	if err != nil {
		if err == redis.Nil {
			return "", nil
		}

		return "", fmt.Errorf("failed to get data from redis: %v", err)
	}

	return value, nil
}

func (s *RequestExchangeTokenRepository) GetHealthCheck() healthcheck.Result {
	name := "redis"
	status := healthcheck.StatusOK
	start := time.Now()

	pong, err := s.client.Ping().Result()
	if err != nil {
		log.Printf("ping to redis failed: %v", err)
		status = healthcheck.StatusError
	} else {
		log.Printf("ping to redis successful: %s", pong)
	}

	return healthcheck.Result{
		Name:         name,
		Status:       status,
		ResponseTime: time.Since(start).Seconds(),
		HealthChecks: nil,
	}
}
