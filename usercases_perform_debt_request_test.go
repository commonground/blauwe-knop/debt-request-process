// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package debtrequestprocess_test

import (
	"errors"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"debtrequestprocess"
	"debtrequestprocess/mock"
)

func Test_UseCase_PerformDebtRequest(t *testing.T) {
	type fields struct {
		schemeRepository               debtrequestprocess.SchemeRepository
		requestExchangeTokenRepository debtrequestprocess.RequestExchangeTokenRepository
		debtRequestRepository          debtrequestprocess.DebtRequestRepository
		tokenGenerator                 debtrequestprocess.TokenGenerator
	}
	type args struct {
		oins         []string
		publicKeyPEM string
	}
	tests := []struct {
		name                  string
		fields                fields
		args                  args
		want                  *debtrequestprocess.DebtRequest
		expectedExchangeToken string
		expectedError         error
	}{
		{
			name: "invalid public key provided",
			fields: fields{
				schemeRepository: func() debtrequestprocess.SchemeRepository {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					repo := mock.NewMockSchemeRepository(ctrl)
					return repo
				}(),
				debtRequestRepository: func() debtrequestprocess.DebtRequestRepository {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					repo := mock.NewMockDebtRequestRepository(ctrl)
					return repo
				}(),
				tokenGenerator: func() debtrequestprocess.TokenGenerator {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					repo := mock.NewMockTokenGenerator(ctrl)
					return repo
				}(),
			},
			args: args{
				[]string{"mock-oin"},
				"invalid public key",
			},
			want:                  nil,
			expectedError:         debtrequestprocess.ErrInvalidPublicKeyPEM,
			expectedExchangeToken: "",
		},
		{
			name: "for a non existing organization",
			fields: fields{
				schemeRepository: func() debtrequestprocess.SchemeRepository {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					repo := mock.NewMockSchemeRepository(ctrl)

					repo.EXPECT().GetOrganizationByOIN("mock-oin").Return(nil, nil).AnyTimes()
					return repo
				}(),
				debtRequestRepository: func() debtrequestprocess.DebtRequestRepository {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					repo := mock.NewMockDebtRequestRepository(ctrl)
					return repo
				}(),
				tokenGenerator: func() debtrequestprocess.TokenGenerator {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					repo := mock.NewMockTokenGenerator(ctrl)
					return repo
				}(),
			},
			args: args{
				[]string{"mock-oin"},
				dummyPublicKeyPem,
			},
			want:                  nil,
			expectedError:         debtrequestprocess.ErrOrganizationNotInScheme,
			expectedExchangeToken: "",
		},
		{
			name: "failed to get the organization from the scheme",
			fields: fields{
				schemeRepository: func() debtrequestprocess.SchemeRepository {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					repo := mock.NewMockSchemeRepository(ctrl)

					repo.EXPECT().GetOrganizationByOIN("mock-oin").Return(nil, errors.New("arbitrary error")).AnyTimes()
					return repo

				}(),
				debtRequestRepository: func() debtrequestprocess.DebtRequestRepository {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					repo := mock.NewMockDebtRequestRepository(ctrl)
					return repo
				}(),
				tokenGenerator: func() debtrequestprocess.TokenGenerator {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					repo := mock.NewMockTokenGenerator(ctrl)
					return repo
				}(),
			},
			args: args{
				[]string{"mock-oin"},
				dummyPublicKeyPem,
			},
			want:                  nil,
			expectedError:         errors.New("failed to get organization by oin: arbitrary error"),
			expectedExchangeToken: "",
		},
		{
			name: "failed to save the debt request",
			fields: fields{
				schemeRepository: func() debtrequestprocess.SchemeRepository {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					repo := mock.NewMockSchemeRepository(ctrl)

					repo.EXPECT().GetOrganizationByOIN("mock-oin").Return(&debtrequestprocess.Organization{
						OIN:        "mock-oin",
						Name:       "name",
						APIBaseURL: "apiBaseUrl",
						LoginURL:   "loginUrl",
					}, nil).AnyTimes()

					return repo
				}(),
				debtRequestRepository: func() debtrequestprocess.DebtRequestRepository {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					repo := mock.NewMockDebtRequestRepository(ctrl)

					repo.EXPECT().Create(debtrequestprocess.DebtRequest{
						Organizations: []*debtrequestprocess.Organization{{
							OIN:        "mock-oin",
							Name:       "name",
							APIBaseURL: "apiBaseUrl",
							LoginURL:   "loginUrl",
						},
						},
						PublicKeyPEM: dummyPublicKeyPem,
					}).Return(nil, errors.New("arbitrary error")).AnyTimes()
					return repo
				}(),
				tokenGenerator: generateDefaultTokenRepo(t),
			},
			args: args{
				[]string{"mock-oin"},
				dummyPublicKeyPem,
			},
			want:                  nil,
			expectedError:         errors.New("failed to save debt request: arbitrary error"),
			expectedExchangeToken: "",
		},
		{
			name: "failed to add the request exchange token",
			fields: fields{
				schemeRepository: func() debtrequestprocess.SchemeRepository {
					repo := generateSchemeRepo(t)

					repo.EXPECT().GetOrganizationByOIN("mock-oin").Return(&debtrequestprocess.Organization{
						OIN:        "mock-oin",
						Name:       "name",
						APIBaseURL: "apiBaseUrl",
						LoginURL:   "loginUrl",
					}, nil).AnyTimes()

					return repo
				}(),
				requestExchangeTokenRepository: func() debtrequestprocess.RequestExchangeTokenRepository {
					repo := generateRequestExchangeTokenRepository(t)
					repo.EXPECT().AddRequestExchangeToken("mock-id", "mock-request-exchange-token").Return(errors.New("arbitrary error")).AnyTimes()
					return repo
				}(),
				debtRequestRepository: func() debtrequestprocess.DebtRequestRepository {
					repo := generateDebtRequestRepository(t)
					repo.EXPECT().Create(debtrequestprocess.DebtRequest{
						PublicKeyPEM: dummyPublicKeyPem,
						Organizations: []*debtrequestprocess.Organization{{
							OIN:        "mock-oin",
							Name:       "name",
							APIBaseURL: "apiBaseUrl",
							LoginURL:   "loginUrl",
						},
						},
					}).Return(&debtrequestprocess.DebtRequest{
						Id:           "mock-id",
						PublicKeyPEM: dummyPublicKeyPem,
						Organizations: []*debtrequestprocess.Organization{{
							OIN:        "mock-oin",
							Name:       "name",
							APIBaseURL: "apiBaseUrl",
							LoginURL:   "loginUrl",
						},
						},
					}, nil).AnyTimes()

					return repo
				}(),
				tokenGenerator: func() debtrequestprocess.TokenGenerator {
					repo := generateDefaultTokenRepo(t)
					repo.EXPECT().GenerateRequestExchangeToken().Return("mock-request-exchange-token").AnyTimes()
					return repo
				}(),
			},
			args: args{
				[]string{"mock-oin"},
				dummyPublicKeyPem,
			},
			want:                  nil,
			expectedError:         errors.New("failed to add request exchange token: arbitrary error"),
			expectedExchangeToken: "",
		},
		{
			name: "happy flow",
			fields: fields{
				schemeRepository: func() debtrequestprocess.SchemeRepository {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					repo := mock.NewMockSchemeRepository(ctrl)

					repo.EXPECT().GetOrganizationByOIN("mock-oin").Return(&debtrequestprocess.Organization{
						OIN:        "mock-oin",
						Name:       "name",
						APIBaseURL: "apiBaseUrl",
						LoginURL:   "loginUrl",
					}, nil).AnyTimes()
					return repo
				}(),
				debtRequestRepository: func() debtrequestprocess.DebtRequestRepository {
					repo := generateDebtRequestRepository(t)
					repo.EXPECT().Create(debtrequestprocess.DebtRequest{
						PublicKeyPEM: dummyPublicKeyPem,
						Organizations: []*debtrequestprocess.Organization{{
							OIN:        "mock-oin",
							Name:       "name",
							APIBaseURL: "apiBaseUrl",
							LoginURL:   "loginUrl",
						},
						},
					}).Return(&debtrequestprocess.DebtRequest{
						Id:           "mock-id",
						PublicKeyPEM: dummyPublicKeyPem,
						Organizations: []*debtrequestprocess.Organization{{
							OIN:        "mock-oin",
							Name:       "name",
							APIBaseURL: "apiBaseUrl",
							LoginURL:   "loginUrl",
						},
						},
					}, nil).AnyTimes()

					return repo
				}(),
				requestExchangeTokenRepository: func() debtrequestprocess.RequestExchangeTokenRepository {
					repo := generateRequestExchangeTokenRepository(t)
					repo.EXPECT().AddRequestExchangeToken("mock-id", "mock-request-exchange-token").AnyTimes()
					return repo
				}(),
				tokenGenerator: func() debtrequestprocess.TokenGenerator {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					repo := mock.NewMockTokenGenerator(ctrl)
					repo.EXPECT().GenerateRequestExchangeToken().Return("mock-request-exchange-token").AnyTimes()
					return repo
				}(),
			},
			args: args{
				[]string{"mock-oin"},
				dummyPublicKeyPem,
			},
			want: &debtrequestprocess.DebtRequest{
				Id: "mock-id",
				Organizations: []*debtrequestprocess.Organization{{
					OIN:        "mock-oin",
					Name:       "name",
					APIBaseURL: "apiBaseUrl",
					LoginURL:   "loginUrl",
				},
				},
				PublicKeyPEM: dummyPublicKeyPem,
			},
			expectedError:         nil,
			expectedExchangeToken: "mock-request-exchange-token",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := debtrequestprocess.NewUseCases(
				tt.fields.requestExchangeTokenRepository,
				tt.fields.debtRequestRepository,
				tt.fields.schemeRepository,
				generateLoginService(t),
				tt.fields.tokenGenerator,
			)
			got, exchangeToken, err := d.PerformDebtRequest(tt.args.oins, tt.args.publicKeyPEM)
			if tt.expectedError != nil {
				assert.Error(t, err, tt.expectedError.Error())
			} else {
				assert.Nil(t, err)
			}

			assert.Equal(t, tt.expectedExchangeToken, exchangeToken)
			assert.Equal(t, tt.want, got)
		})
	}
}
