package debtrequestprocess

import (
	"math/rand"
	"time"
)

type RandomTokenGenerator struct {
	letters []rune
}

func NewRandomTokenGenerator() *RandomTokenGenerator {
	rand.Seed(time.Now().Unix())
	return &RandomTokenGenerator{
		letters: []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"),
	}
}

func (r *RandomTokenGenerator) generateToken(length int) string {
	b := make([]rune, length)
	for i := range b {
		b[i] = r.letters[rand.Intn(len(r.letters))]
	}
	return string(b)
}

func (r *RandomTokenGenerator) GenerateRequestExchangeToken() string {
	return r.generateToken(16)
}
