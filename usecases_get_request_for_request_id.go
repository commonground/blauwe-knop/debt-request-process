// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package debtrequestprocess

import (
	"errors"
	"fmt"
)

var ErrRequestDoesNotExist = errors.New("invalid request id")

func (a *UseCases) GetDebtRequestForDebtRequestID(debtRequestID string) (*DebtRequest, error) {
	debtRequest, err := a.debtRequestRepository.Get(debtRequestID)
	if err != nil {
		return nil, fmt.Errorf("failed to get debt request by id: %v", err)
	}

	if debtRequest == nil {
		return nil, ErrRequestDoesNotExist
	}

	return debtRequest, nil
}
